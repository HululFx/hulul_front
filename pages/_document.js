import Document, { Html, Head, Main, NextScript } from 'next/document'

var dir;
class MyDocument extends Document {
  static async getInitialProps(ctx) {
    dir = ctx.locale == "ar" ? "rtl" : "ltr"
    const originalRenderPage = ctx.renderPage

    // Run the React rendering logic synchronously
    ctx.renderPage = () =>
      originalRenderPage({
        // Useful for wrapping the whole react tree
        enhanceApp: (App) => App,
        // Useful for wrapping in a per-page basis
        enhanceComponent: (Component) => Component,
      })

    // Run the parent `getInitialProps`, it now includes the custom `renderPage`
    const initialProps = await Document.getInitialProps(ctx)

    return initialProps
  }
  render() {
    return (
      <Html dir={dir} >
        <Head >
          {dir === "rtl" ?
            <link href="https://cdnjs.cloudflare.com/ajax/libs/rsuite/5.11.0/rsuite-rtl.min.css" rel="stylesheet" />
            :
            <link href='https://cdnjs.cloudflare.com/ajax/libs/rsuite/5.11.0/rsuite.min.css' rel="stylesheet" />
          }
          <meta name="description" content="Hulul Fx was established in 2020 as a distinguished company, developing integrated systems and applications to meet the need of regional markets in the area of information technology by providing integrated solutions to manage the business and meet the requirements for the different sectors.
          The company was since its establishment focused on excellence in building integrated systems, using the latest technology to meet the needs of different sectors in the computerization of its work. Starting from the company’s mission, the company attracts skilled individuals, who are capable of dealing with the technical information effectively, to achieve highly competitive solutions for the company’s target market.
          In addition, the company has made excellence in customer service which is one of the key pillars of the mechanisms of local technical support provided by the company to its customers. The company adopts the latest standard for the development of systems which is provided by relational databases and open source systems to achieve flexibility in building the required solutions.
          The company has achieved the success of the deployment of its solutions in the market, where its application has been used in United Kingdom, and Turkey ." />
          <link rel="apple-touch-icon" sizes="60x60" href="../static/apple-touch-icon.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="../static/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="../static/favicon-16x16.png" />
          <link rel="manifest" href="../static/site.webmanifest" />
          <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
          <meta name="msapplication-TileColor" content="#ffffff" />
          <meta name="theme-color" content="#ffffff" />
          {/* <!-- ================ Safe generic defaults ================ --> */}
          <meta property="og:image" content="../static/template-Landscape-min.png" />
          <meta property="og:image:width" content={1024} />
          <meta property="og:image:height" content={512} />


          <meta property="og:image" content="../static/template-Portrait-min.png" />
          <meta property="og:image:width" content="800" />
          <meta property="og:image:height" content="1200" />

          {/* <!-- ================ Facebook ================ --> */}
          {/* <!-- Facebook will give the user the choice to choose one of the og:image thumbnails. --> */}

          <meta property="og:image" content="../static/template-Facebook-min.png" />
          {/* <!-- Recommended image for Facebook. (Max. 8MB)--> */}
          <meta property="og:image:width" content="1200" />
          <meta property="og:image:height" content="630" />

          {/* <!--  ================ Linkedin ================ --> */}
          <meta property="og:image" content="../static/template-Linkedin-min.png" />
          <meta property="og:image:width" content="180" />
          <meta property="og:image:height" content="110" />

          {/* <!-- ================ Pinterest ================ --> */}
          <meta property="og:image" content="../static/template-Pinterest-min.png" />
          <meta property="og:image:width" content="736" />
          <meta property="og:image:height" content="1128" />

        </Head>

        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
export default MyDocument
