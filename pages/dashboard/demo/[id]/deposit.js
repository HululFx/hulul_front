import React, { useState, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { MoneyRecive, ArrowLeft } from 'iconsax-react';
import Link from "next/link"
import CardAccountTop from "@/ui/CardAccountTop"
import { ButtonTheme, CopyToClip, Slider,Slider2, Error, Loading, NoData } from "@/ui"
import { CustumnCheckbox } from "@/form"
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { depositDemoAccount, userDemoAccountWithoutPagination } from "apiHandle"
import useSWR from 'swr'
import Head from 'next/head'
import { useRouter } from "next/router"
import useAuth from 'libs/useAuth'
import useWindowSize from "hooks/use-window";
export default function Deposit() {
    const size = useWindowSize();
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const { t, lang } = useTranslation("dashboard")
  const router = useRouter();
  const [loadingButton, setLoadingButton] = useState(false)
  const [change, setChange] = useState(false)

  const handleSubmit = (values) => {
    setLoadingButton(true);
    depositDemoAccount({
      login:currentAccount.login,
      values: values,
      success: () => {setLoadingButton(false);router.push("/dashboard")},
      error: () => setLoadingButton(false)
    })
  };
  const [currentAccount, setCurrentAccount] = useState()
  const [currentId, setCurrentId] = useState(router.query.account ? router.query.account : router.query.id)
  const [allAccounts, setAllAccounts] = useState()
  const { data, error } = useSWR(userDemoAccountWithoutPagination())
  useEffect(() => {
    if (data) {
      setCurrentAccount(data.demo_accounts_Informations.filter((account) => +account.id === +currentId)[0] || { error: "there is no account" })
      setAllAccounts(data.demo_accounts_Informations)
    }
  }, [data])
  const handleChooseIndexSlide = (i) => {
    router.push({
      pathname: router.asPath.split("?")[0],
      query: { account: data.demo_accounts_Informations[i].id },

    }, undefined, { scroll: false })
    setCurrentAccount(data.demo_accounts_Informations[i])
    setCurrentId(data.demo_accounts_Informations[i].id)
  }
  if (isLoading || !user){
    return <Loading  page={true}/>
}
  return (
    <>
      <Head>
        <title>{t("deposit")} | {t("common:website_name")}</title>
      </Head>
      <div className="p-3 bg-white rounded-lg lg:p-8 sm:p-4 dark:bg-dark-white md:rounded-xl">
        <div className="flex items-center justify-between mb-6">
          <div className="flex items-center gap-2 ">
            <div className=" icon-container">
              <MoneyRecive className="w-4 h-4 [color:rgb(var(--primary-color))] -400 lg:w-8 lg:h-8" />
            </div>
            <h1 className="block text-lg font-bold text-black lg:text-3xl dark:text-white">{t("deposit")}</h1>
          </div>
          <Link href="/dashboard" >
            <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl">
              <ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
            </a>
          </Link>
        </div>
        {error ? <Error apiMessage={error} />
          : (!data || !currentAccount) ? <div className="flex justify-center items-center min-h-[calc(100vh_-_20rem)]">< Loading /></div>
            : currentAccount.error === "there is no account" ? <NoData text={t("sorry_the_account_is_not_present")} />
              : <div className="py-4 mx-auto w-[43.75rem] max-w-full ">
                <div className="mb-4 ">
                  <h2 className="mb-4 text-lg text-gray-500">{t("the_deposit_for_calculating")}&nbsp;&nbsp;<span className="text-danger ">({t("demo_account")})</span></h2>
                  <div className="mb-8  lg:mb-0">
                  {size.width > process.env.md ?
                    <Slider data={allAccounts} currentAccount={currentAccount} type="demo" chooseSlide={allAccounts.indexOf(currentAccount)} handleChooseIndexSlide={handleChooseIndexSlide} />
                    :
                  <Slider2 data={allAccounts} currentAccount={currentAccount} type="demo" chooseSlide={allAccounts.indexOf(currentAccount)} handleChooseIndexSlide={handleChooseIndexSlide} />
                  }
                    </div>
                </div>
                <div className=" w-[31.25rem] max-w-full rtl:ml-auto ltr:mr-auto">
                  <h2 className="mb-4 text-lg text-gray-500 ">{t("fast_deposit")}</h2>
                  <Formik
                    validationSchema={() => Yup.object().shape({
                      Balanced: Yup.number().required(t('please_choose_a_fast_deposit_value')),
                    })}
                    initialValues={{ Balanced: "" }}
                    onSubmit={handleSubmit}
                  >
                    {( props ) => {
                      props.dirty && setChange(true)
                      return(
                      <Form>
                        <div className="grid grid-cols-2 gap-4 mb-4 lg:grid-cols-3">
                          <CustumnCheckbox name="Balanced" value="1000" type="radio" number />
                          <CustumnCheckbox name="Balanced" value="3000" type="radio" number />
                          <CustumnCheckbox name="Balanced" value="5000" type="radio" number />
                        </div>
                        <ErrorMessage name="Balanced" component="span" className="mb-4 text-danger" />
                        <ButtonTheme color="primary"disabled={!change} block className="p-4 mt-8 text-xl">{t("deposit_now")}</ButtonTheme>
                      </Form>
                    )}}
                  </Formik>
                </div>
              </div>
        }
      </div >
    </>
  )
}
