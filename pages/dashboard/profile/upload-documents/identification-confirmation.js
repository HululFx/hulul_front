import React, { useState, useEffect ,useContext} from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ProfileContainer } from "@/container"
import { ID, Passport, DrivingCard } from "public/svg"
import { Verify, ArrowLeft } from 'iconsax-react'
import { UploadDraggableImage } from '@/form'
import { Formik } from "formik";
import { ButtonTheme, Error, Loading } from "@/ui"
import {CheckUserData, profilePersonalIdentificationConfirmation ,profileIdentCheck ,profileDocs} from "apiHandle"
import Head from 'next/head'
import useSWR, { useSWRConfig } from 'swr'
import useAuth from 'libs/useAuth'
import PermitionContext from "store/permition-context";
export default function IdentificationConfirmation() {
    const ctx = useContext(PermitionContext);
  const {user, isLoading} = useAuth({middleware: 'auth'})
  const { t, lang } = useTranslation("profile")
  const [choose, setChoose] = useState(-1)
  const [done, setDone] = useState(false)
  const [accepted, setAccepted] = useState(false)
  const [change, setChange] = useState(false)
  const [chooseValue, setChooseValue] = useState()
  const [loadingButton, setLoadingButton] = useState(false)
const { mutate } = useSWRConfig();
  const options = [
    { title: t("passport"), icon: <Passport />, value: "passport" },
    { title: t("id"), icon: <ID />, value: "id" },
    { title: t("driving_card"), icon: <DrivingCard />, value: "driving_card" },
  ]
  const roles = [
    , t("your_personal_picture")
    , t("your_full_name")
    , t("date_of_birth")
    , t("expiration_date")
    , t("card_number")
    , t("your_birthplace")
  ]
  const onSubmit = (values) => {
    setLoadingButton(true);
    profilePersonalIdentificationConfirmation({
      values: { ...values, document_type: chooseValue },
      success: () => { setLoadingButton(false); setDone(true);mutate(CheckUserData()) },
      error: () => setLoadingButton(false)
    })
  }
  // const { data, error } = useSWR( profileIdentCheck())
  const { data:docs, error:error2 } = useSWR( profileDocs())
  useEffect(() => {
    if(ctx.userInfo.identity_confirmation){
      docs && setAccepted(docs.Documents.some(s=>s["type"]=="identity confirmation" && s["document_status"] =="accepted" ))
      docs &&  setDone(docs.Documents.some(s=>s["type"]=="identity confirmation" && s["document_status"] =="pending" ))
    }
  }, [docs])
   if (isLoading || !user){
    return <Loading  page={true}/>
}
  return (
    <>
     <Head>
        <title>{t("identification_confirmation")} | {t("common:website_name")}</title>
      </Head>
    <ProfileContainer tab={"uploadDocuments"}>
      <Formik initialValues={{ first_document: "", second_document: "", document_type: chooseValue, type: "identity confirmation" }}
        onSubmit={onSubmit} >
        {(props) => {
          props.dirty && setChange(true)
          return (
            <form onSubmit={props.handleSubmit} className="min-h-inherit">
               {( error2) ? <Error apiMessage={error2} />
            : !docs  ? <div className="flex items-center justify-center mb-8 h-full flex-col	min-h-inherit"><Loading /></div>
              :!(done || accepted) ?
                <>
                  {choose === -1 ? <>
                    <h2 className="text-center lg:text-2xl text-lg mb-8">{t("choose_how_to_confirm_your_personal_identity")}</h2>
                    <ul className="grid md:grid-cols-3 grid-cols-1 justify-between gap-6 mb-10">
                      {options.map((option, index) => (
                        <li key={index} className="bg-secondary dark:bg-dark-secondary  rounded-xl lg:w-full w-[16rem] max-w-[90%] mx-auto text-center flex justify-center items-center">
                          <button className="md:py-6 py-4 lg:px-8 px-6" onClick={() => { setChoose(index);; setChooseValue(option.value) }}>
                            <div className="mb-4 [color:rgb(var(--primary-color))] ">{option.icon}</div>
                            <div className="lg:text-xl text-base">{option.title} </div>
                          </button>
                        </li>
                      ))}
                    </ul>
                  </>
                    :
                    <>
                      <div className="flex  mb-8 justify-between items-center ">
                        <h2 className="text-center lg:text-2xl text-lg">{t("we_need_to_confirm_your_identity")}</h2>
                        {!change && <button onClick={() => setChoose(-1)} className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl">
                          <ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" :"transform rotate-180"}`} />
                        </button>}

                      </div>

                      {choose === 1 ?
                        <div className="grid md:grid-cols-2 grid-cols-1 lg:gap-8 grid-4">
                          <UploadDraggableImage name="first_document" fileName={t("frontFace")} dirty={change} />
                          <UploadDraggableImage name="second_document" fileName={t("backtFace")} dirty={change} />
                        </div>
                        :
                        <UploadDraggableImage name="first_document" fileName={<bdi><span>{t("raise_a")}</span>{" "}<span>{options[choose].title}</span></bdi>} dirty={change} />
                      }
                      {(change && ((choose != 1 && props.values.first_document) || (choose == 1 && props.values.first_document && props.values.second_document))) && <ButtonTheme color="primary" as="button" type="submit" size="md" block className="mt-8 mb-10 text-center xs:my-4" loading={loadingButton} disabled={!change}>
                        {t('upload_the_file')}
                      </ButtonTheme>

                      }

                    </>
                  }
                  <h2 className="lg:text-lg mb-6 text-sm">{t("make_sure_the_following_information_is_clearly_showing")}</h2>
                  <div className="mb-10">
                    <ul className="grid xl:grid-cols-5 grid-cols-1 sm:grid-cols-3">
                      {roles.slice(0, 4).map((role, index) => (
                        <li key={index} className="mx-4 mb-6  text-xs  relative before:[background:rgba(var(--primary-color),1)] before:rounded-full before:w-3 before:h-3 before:absolute  rtl:before:-right-5 ltr:before:-left-5 before:top-1/2 before:transform before:-translate-y-1/2">{role}</li>
                      ))}
                    </ul>
                    <ul className="grid xl:grid-cols-5 grid-cols-1 sm:grid-cols-3  ">
                      {roles.slice(-3).map((role, index) => (
                        <li key={index} className="mx-4 mb-6  text-xs  relative before:[background:rgba(var(--primary-color),1)] before:rounded-full before:w-3 before:h-3 before:absolute  rtl:before:-right-5 ltr:before:-left-5 before:top-1/2 before:transform before:-translate-y-1/2">{role}</li>
                      ))}
                    </ul>
                  </div>
                </>
                :
                <div className="flex items-center justify-center mb-8 h-full flex-col	min-h-inherit">
                  <Verify size="170" className="mx-auto mb-10 text-success more-linear" />
                  <p className='text-center mb-8 text-lg font-bold w-[23.75rem] max-w-full mx-auto'>{
                    accepted ? 
                  t("wonderful_your_identity_has_been_confirmed")
                  :
                  t("fabulous_the_files_will_be_reviewed_by_our_team_and_we_inform_you_in_the_event_of_approval_or_re_upload_a_document")
                  }</p>
                </div>
              }
            </form>
          )
        }}
      </Formik>
    </ProfileContainer >
    </>
  )
}
