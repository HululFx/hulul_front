import React, { useState } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ProfileContainer } from "@/container"
import { Location, ClipboardText, Lock1, Call, Add } from 'iconsax-react'
import Link from "next/link"
import Head from 'next/head'
import { ButtonTheme, Loading } from "@/ui"
import { CreateBankAccount, CardAccountVisa } from "@/ui"
import useAuth from 'libs/useAuth'
export default function ConfirmTheAddress() {
    const { user, isLoading } = useAuth({ middleware: 'auth' })
    const { t, lang } = useTranslation("profile")
    if (isLoading || !user){
        return <Loading  page={true}/>
    }
    return (
        <>
            <Head>
                <title>{t("payment_cards")} | {t("common:website_name")}</title>
            </Head>
            <ProfileContainer tab={"bankAccount"}>
                <div className="mx-auto ">
                    <h2 className="mt-8 mb-4 text-xl text-gray-600">{t("your_payment_card")}</h2>
                    <div className="grid grid-cols-1 gap-10 lg:grid-cols-2">
                        <CreateBankAccount text={t("common:add_a_new_card")} href="/" />
                        <CardAccountVisa color="bg-[#505BBD]" />
                        <CardAccountVisa color="bg-[#DD4A90]" />
                        <CardAccountVisa color="bg-[#C58448]" />
                    </div>
                </div>
            </ProfileContainer>
        </>
    )
}

