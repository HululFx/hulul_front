import React, { useState, useEffect, useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ProfileContainer } from "@/container"
import { UploadImage, InputIcon, Input, InputCity, InputDate, SelectWIthHead, InputPhone } from "@/form"
import { ButtonTheme, Error, Loading } from "@/ui"
import { Formik } from "formik";
import { Sms, Lock, Eye, EyeSlash, Profile, Star1, Location, Courthouse, MedalStar, Call, Whatsapp, CallAdd } from 'iconsax-react';
import { profilePersonalUserContactInformation, profilePersonalCompanyContactInformation } from "apiHandle"
import { useProfilePersonal } from "hooks/use-with-swr"
import useSWR, { useSWRConfig } from 'swr'
import { CheckUserData, getEmailCode, getPhoneCode } from "apiHandle"
import Head from 'next/head'
import PermitionContext from "store/permition-context";
import useAuth from 'libs/useAuth'
import { useRouter } from 'next/router'
import { Loader } from 'rsuite';

export default function ContactInformation() {
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const { t, lang } = useTranslation("profile");
  const { mutate } = useSWRConfig();
  const ctx = useContext(PermitionContext);
  const [loadingButton, setLoadingButton] = useState(false)
  const [change, setChange] = useState(false)
  const [sendEmail, setSendEmail] = useState(false)
  const [sendPhone, setSendPhone] = useState(false)
  const [role, setRole] = useState()
  const router = useRouter()
  useEffect(() => {
    if (typeof window !== "undefined") {
      setRole(localStorage.userType);
    }
  }, [role]);
  const onSubmitUser = (values) => {
    setLoadingButton(true);
    profilePersonalUserContactInformation({
      values: values,
      success: () => { setLoadingButton(false); mutate(CheckUserData()) },
      error: () => setLoadingButton(false)
    })
  }
  const onSubmitCompany = (values) => {
    setLoadingButton(true);
    profilePersonalCompanyContactInformation({
      values: values,
    success: () => {setLoadingButton(false); },
      error: () => setLoadingButton(false)
    })
  }
  const { data, error } = useProfilePersonal(role);
  if (isLoading || !user) {
    return <Loading page={true} />
  }
  return (
    <>
      <Head>
        <title>{t("contact_information")} | {t("common:website_name")}</title>
      </Head>
      <ProfileContainer tab={"personal"} >
        <div className="w-[31.25rem] max-w-full mx-auto min-h-inherit">
          {error ? <Error apiMessage={error} />
            : !data ? <div className="flex items-center justify-center mb-8 h-full flex-col	min-h-inherit"><Loading /></div>
              : <Formik
                enableReinitialize
                initialValues=
                {role === "user" ?
                  {
                    email: data.user_info.email ? data.user_info.email : ctx.userInfo.email_field,
                    phone: data.user_info.phone ? data.user_info.phone : ctx.userInfo.phone_field,
                    whatsapp_number: data.user_info.whatsapp_number,
                    second_phone: data.user_info.second_phone
                  } :
                  {
                    email: data.company_info.email ? data.company_info.email : ctx.userInfo.email_field,
                    phone: data.company_info.phone ? data.company_info.phone : ctx.userInfo.phone_field,
                    whatsapp_number: data.company_info.whatsapp_number,
                    second_phone: data.company_info.second_phone
                  }

                }
                onSubmit={role === "user" ? onSubmitUser : onSubmitCompany} >
                {(props) => {
                  props.dirty && setChange(true)
                  return (
                    <form onSubmit={props.handleSubmit}>
                      <InputIcon icon={<Sms className="[color:rgb(var(--primary-color))] " />} className="mb-4 mt-8">
                        <Input name="email" type="email" placeholder={t('email')} />
                      </InputIcon>
                      <div className="flex items-center justify-between mb-12">
                        {(props.values.email && !ctx.userInfo.email  ) &&
                          <span className={`flex flex-row-reverse -mt-2 gap-4 cursor-pointer ${sendEmail ? "text-gray-400 events-none" : "[color:rgb(var(--primary-color))]"}`}
                            onClick={() => {
                              setSendEmail(true)
                              getEmailCode({
                                success: () => { router.push(`/auth/enter-phone-code?email=${props.values.email}&verify=true`) },
                                error: () => { setSendEmail(false) },
                                email: `${props.values.email}`
                              })
                            }}>
                              <span>{t("email_confirmation")}</span>
                              <span>{sendEmail && <Loader size="xs" />}</span>
                          </span>}
                      </div>
                      <InputIcon icon={<Call className="[color:rgb(var(--primary-color))] " />}>
                        <InputPhone name="phone" type="tel" placeholder={t('phone_number')} defaultValue={props.values.phone} />
                      </InputIcon>
                      <div className="flex items-center justify-between mb-6">
                        {(props.values.phone && !ctx.userInfo.phone ) && <span
                          className={`flex flex-row-reverse -mt-2 gap-4  cursor-pointer ${sendPhone ? "text-gray-400 events-none" : "[color:rgb(var(--primary-color))]"}`}
                          onClick={
                            () => {
                              setSendPhone(true)
                              getPhoneCode({
                                success: () => { router.push(`/auth/enter-phone-code?phone=${props.values.phone}&verify=true`) },
                                error: () => { setSendPhone(false) },
                                phone: `${props.values.phone}`
                              })
                            }
                          }
                        >
                          <span>
                            {t("confirm_the_phone_number")}
                          </span>
                          <span>
                            {sendPhone && <Loader size="xs" />}
                          </span>

                        </span>}
                      </div>

                      <InputIcon icon={<Whatsapp className="[color:rgb(var(--primary-color))] " />}>
                        <InputPhone name="whatsapp_number" type="tel" placeholder={t('whats_app_number')} defaultValue={props.values.whatsapp_number} />
                      </InputIcon>
                      <InputIcon icon={<CallAdd className="[color:rgb(var(--primary-color))] " />}>
                        <InputPhone name="second_phone" type="tel" placeholder={t('add_another_number')} defaultValue={props.values.second_phone} />
                      </InputIcon>

                      <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-12 text-center xs:my-4" loading={loadingButton} disabled={!change}>
                        {t('save')}
                      </ButtonTheme>
                    </form>
                  )
                }}
              </Formik>}
        </div>
      </ProfileContainer >
    </>
  )
}
