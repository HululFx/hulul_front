import React, { useState, useEffect, useContext } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { ProfileContainer } from "@/container"
import { UploadImage, InputIcon, Input, InputCity, InputDate, SelectWIthHead } from "@/form"
import { ButtonTheme, Error, Loading } from "@/ui"
import { Formik } from "formik";
import { Sms, Lock, Eye, EyeSlash, Profile, Star1, Location, Courthouse, MedalStar } from 'iconsax-react';
import { profilePersonalProfileUserPersonly, profilePersonalProfileCompanyPersonly } from "apiHandle"
import { useProfilePersonal } from "hooks/use-with-swr"
import Head from 'next/head'
import useSWR, { useSWRConfig } from 'swr'
import { CheckUserData } from "apiHandle"
import PermitionContext from "store/permition-context";
import useAuth from 'libs/useAuth'
export default function ProfilePersonly() {
  const { user, isLoading } = useAuth({ middleware: 'auth' })
  const ctx = useContext(PermitionContext);
  const { t, lang } = useTranslation("profile");
  const { mutate } = useSWRConfig();
  const [role, setRole] = useState()
  useEffect(() => {
    if (typeof window !== "undefined") {
      setRole(localStorage.userType);
    }
  }, [role]);
  const [loadingButton, setLoadingButton] = useState(false)
  const [change, setChange] = useState(false)
  const onSubmitUser = (values) => {
    setLoadingButton(true);
    profilePersonalProfileUserPersonly({
      values: values,
      success: () => { setLoadingButton(false); mutate(CheckUserData()) },
      error: () => setLoadingButton(false)
    })
  }
  const onSubmitCompany = (values) => {
    setLoadingButton(true);
    profilePersonalProfileCompanyPersonly({
      values: values,
      success: () => { setLoadingButton(false); mutate(CheckUserData()) },
      error: () => setLoadingButton(false)
    })
  }

  const { data, error } = useProfilePersonal(role);
  if (isLoading || !user) {
    return <Loading page={true} />
  }
  return (
    <>
      <Head>
        <title>{t("profile_personly")} | {t("common:website_name")}</title>
      </Head>
      <ProfileContainer tab={"personal"} >
        <div className="w-[31.25rem] max-w-full mx-auto min-h-inherit">
          {error ? <Error apiMessage={error} />
            : !data ? <div className="flex items-center justify-center mb-8 h-full flex-col	min-h-inherit"><Loading /></div>
              : role === "user" ?
                <Formik initialValues={
                  { user_img: "", full_name: data.user_info.full_name ? data.user_info.full_name : ctx.userInfo.name, Birth_date: data.user_info.Birth_date, Birth_location: data.user_info.Birth_location }} onSubmit={onSubmitUser} >
                  {(props) => {
                    props.dirty && setChange(true)
                    return (
                      <form onSubmit={props.handleSubmit}>
                        <UploadImage name="user_img" defaultImg={data.user_info && data.user_info.user_img} />
                        <InputIcon icon={<Profile className="[color:rgb(var(--primary-color))] " />}>
                          <Input name="full_name" type="text" placeholder={t('full_name')} />
                        </InputIcon>
                        <InputIcon icon={<Star1 className="[color:rgb(var(--primary-color))] " />}>
                          <InputDate name="Birth_date" type="text" placeholder={t('date_of_birth')} defaultValue={(data.user_info && data.user_info.Birth_date) && new Date(data.user_info.Birth_date)} />
                        </InputIcon>
                        <InputIcon icon={<Location className="[color:rgb(var(--primary-color))] " />}>
                          <InputCity name="Birth_location" type="text" placeholder={t('christmas_state')} defaultValue={props.values.Birth_location} />
                        </InputIcon>
                        <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-12 text-center xs:my-4" loading={loadingButton} disabled={!change}>
                          {t('save')}
                        </ButtonTheme>
                      </form>
                    )
                  }}
                </Formik>
                :
                <Formik initialValues={data.company_info ? { company_img: "", company_name: data.company_info.company_name, representative_name: data.company_info.representative_name, representative_position: data.company_info.representative_position, Created_date: data.company_info.Created_date } :
                  { company_img: "", company_name: "", representative_name: "", representative_position: "", Created_date: "" }} onSubmit={onSubmitCompany}>
                  {(props) => {
                    props.dirty && setChange(true)
                    return (
                      <form onSubmit={props.handleSubmit}>
                        <UploadImage name="company_img" defaultImg={data.company_info && data.company_info.company_img} />
                        <InputIcon icon={<Courthouse className="[color:rgb(var(--primary-color))] " />}>
                          <Input name="company_name" type="text" placeholder={t('the_company_name')} />
                        </InputIcon>
                        <InputIcon icon={<Profile className="[color:rgb(var(--primary-color))] " />}>
                          <Input name="representative_name" type="text" placeholder={t('the_name_of_the_company_representative')} />
                        </InputIcon>
                        <SelectWIthHead name="representative_position" head={<MedalStar className="[color:rgb(var(--primary-color))] " />} options="representative_position" defaultValue={props.values.representative_position} />
                        <InputIcon icon={<Location className="[color:rgb(var(--primary-color))] " />}>
                          <InputDate name="Created_date" type="text" placeholder={t('the_date_of_the_establishment_of_the_company')} defaultValue={(data.company_info && data.company_info.Created_date) && new Date(data.company_info.Created_date)} />
                        </InputIcon>
                        <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-12 text-center xs:my-4" loading={loadingButton} disabled={!change}>
                          {t('save')}
                        </ButtonTheme>
                      </form>
                    )
                  }}
                </Formik>
          }
        </div>
      </ProfileContainer>
    </>
  )
}
