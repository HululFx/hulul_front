import React, { useState } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Add, ArrowLeft, Flag, Cup, Crown1 } from 'iconsax-react';
import Link from "next/link"
import { ButtonTheme, RealTypesAccounts , Loading} from "@/ui"
import { allAccountsTypes } from "apiHandle"
import useSWR from 'swr'
import { useRouter } from 'next/router';
import { changeRealAccountSetting } from "apiHandle"
import Head from 'next/head'
import useAuth from 'libs/useAuth'
export default function ChangeTypeAccount() {
      const {user, isLoading} = useAuth({middleware: 'auth'})
    const router = useRouter();
    const { t  , lang} = useTranslation("dashboard")
    const [accountType, setAccountType] = useState(router.query.account_type)
    const [loadingButton, setLoadingButton] = useState(false)
    const [change, setChange] = useState(1)
    const plans = [
        { icon: <Flag className="text-white" size="30" />, title: t("essential"), pips: "1.5", lowest: "$100", ea: t("no"), volume: "0.01", islamic: t("yes"), value: "1" },
        { icon: <Cup className="text-white" size="30" />, title: t("classic"), pips: "1.2", lowest: "$100", ea: t("no"), volume: "0.01", islamic: t("yes"), value: "2" },
        { icon: <Crown1 className="text-white" size="30" />, title: t("professionalism"), pips: "0.1", lowest: "$200", ea: t("no"), volume: "0.01", islamic: t("yes"), value: "3" }
    ]
    const handleSubmit = (e) => {
        e.preventDefault();
        setLoadingButton(true);
        changeRealAccountSetting({
            values: {
                
                account_id: router.query.id,
                account_type: accountType
            },
            success: () => { setLoadingButton(false); },
            error: () => setLoadingButton(false)
        })
    };
    const { data, error } = useSWR(allAccountsTypes())
     if (isLoading || !user){
    return <Loading  page={true}/>
}
    return (
        <>
         <Head>
        <title>{t("modify_the_type_of_account")} | {t("common:website_name")}</title>
      </Head>
            <div className="lg:p-8  sm:p-4 p-3 bg-white dark:bg-dark-white rounded-lg md:rounded-xl">
                <div className="flex items-center justify-between mb-6">
                    <div className="flex items-center gap-2 ">
                        <div className=" icon-container">
                            <Add className="[color:rgb(var(--primary-color))] -400 lg:w-8 w-4 lg:h-8 h-4" />
                        </div>
                        <h1 className="block lg:text-3xl text-lg font-bold text-black dark:text-white">{t("modify_the_type_of_account")}</h1>
                    </div>

                    <Link href="/dashboard" >
                        <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl">
                            <ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" :"transform rotate-180"}`} />
                        </a>
                    </Link>

                </div>
                <div className="py-4 ">

                    <form onSubmit={handleSubmit}>
                        <div className="mx-auto  w-[56.25rem] max-w-full">
                            <h2 className="mb-0 text-lg text-gray-600 ">{t("account_type")}</h2>

                                <RealTypesAccounts error={error} data={data} accountType={accountType} setAccountType={setAccountType} change={change} setChange={setChange} />
                            <div className="mx-auto  w-[31.25rem] max-w-full">
                                <ButtonTheme type="submit" loading={loadingButton} disabled={change === 1} color="primary" block size="xs" >{t("saving_changes")}</ButtonTheme>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    )
}

