import Head from 'next/head'
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/effect-fade";
// import { motion } from "framer-motion";

import 'react-phone-input-2/lib/style.css'
import "swiper/css/effect-cards";
import 'styles/globals.css'
import { Toaster } from "react-hot-toast";
import useSWR, { SWRConfig } from 'swr'
import axios from "libs/axios";
import { useState } from "react"
import Router from 'next/router'
import NProgress from 'nprogress'

import { CustomProvider } from "rsuite";
import Layout from "@/container/Layout"
Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())
NProgress.configure({ showSpinner: true })
import useTranslation from 'next-translate/useTranslation'
import { PermitionContextProvider } from "store/permition-context";

function MyApp({ Component, pageProps, router }) {
    const { t, lang } = useTranslation("aside");
    const dir = lang === "ar" ? "rtl" : "ltr";

    return (
        <>
            <PermitionContextProvider>
                <SWRConfig
                    revalidateOnMount={false}
                    revalidateOnFocus={false}
                    revalidateOnReconnect={false}
                    revalidateIfStale={false}
                    refreshInterval={0}
                    value={{
                        fetcher: url => axios.get(url).then(res => res.data)
                    }}
                >
                    <CustomProvider rtl={lang === "ar" ? true : false} >
                        <Toaster position="bottom-right" gutter={30} toastOptions={{
                            custom: {
                                duration: 10000,
                            },
                        }} />
                        {/* <motion.div
                            key={router.route}
                            initial="initial"
                            animate="animate"
                            variants={{
                                initial: {
                                    opacity: 0,
                                    // scale:0.95
                                },
                                animate: {
                                    opacity: 1,
                                    // scale:1
                                },
                            }}
                        > */}
                            {Component.getLayout ?
                                <Component {...pageProps} />
                                :
                                <Layout>
                                    <Component {...pageProps} />
                                </Layout>
                            }
                        {/* </motion.div> */}

                    </CustomProvider>
                </SWRConfig>
            </PermitionContextProvider>
        </>
    )
}
export default MyApp
