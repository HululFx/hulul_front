
import { useState, useEffect } from "react"
import Login from "@/container/auth";
import { Input, InputIcon, InputCity, InputPhone, InputCheck } from "@/form"
import useTranslation from 'next-translate/useTranslation'
import { Profile, Courthouse, Sms, Lock, Eye, EyeSlash, Flag, Call } from 'iconsax-react';
import { ButtonTheme, Loading ,LoginFacebookAndGmail } from "@/ui"
import toast from "react-hot-toast";
import { Formik } from "formik";
import * as Yup from "yup";
import Link from "next/link"
import Head from 'next/head'
import useAuth from 'libs/useAuth'
import {loginUser ,successLogin} from "apiHandle"
export default function LoginUser() {
  const [passwordType, setPasswordType] = useState(true)
  const { t, lang } = useTranslation("auth")
  const [loadingButton, setLoadingButton] = useState(false)
  const {  isLoading, user } = useAuth({ middleware: 'guest' })

  
 


  const onSubmit = (values) => {
    setLoadingButton(true);
    loginUser({
      values: values,
      success: (response) => {
        setLoadingButton(false);
        successLogin(response)
      },
      error: (err) => {
        setLoadingButton(false)
      }
    })
  }

  
  if (isLoading || user) {
    return <Loading  page={true}/>
  }
  return (
    <>
      <Head>
        <title>{t("sign_in")} | {t("common:website_name")}</title>
      </Head>
      <Login slider>
        <span className="block mt-12 mb-3 text-gray-400 text-md">{t('glad_you_are_back')}</span>
        <h1 className="mb-8 font-bold leading-none text-h2">{t('welcome_again')}</h1>
        <LoginFacebookAndGmail/>
        <Formik initialValues={{ phone: "", password: "" }} onSubmit={onSubmit} validationSchema={() => Yup.object().shape({
          phone: Yup.number().required(t('please_enter_the_phone')),
          // password: Yup.string().required(t("please_write_the_password")).min(8, t("the_password_should_not_be_less_than_eight_letters"))
          //   .max(12, t("the_password_should_not_exceed_twelve_letters")).matches(/[a-z]/, t("the_password_must_contain_letters")).matches(/[1-9]/, t("the_password_must_contain_numbers"))
        })}>
          {(props) => (
            <form onSubmit={props.handleSubmit}>
            <InputIcon icon={<Call className="[color:rgb(var(--primary-color))] " />}>
              <InputPhone name="phone" type="tel" placeholder={t('phone_number')} />
            </InputIcon>
              <InputIcon icon={<Lock className="[color:rgb(var(--primary-color))] " />} className="password">
                <span role="button" className="absolute transform top-4 rtl:left-4 ltr:right-4 rtl:md:left-3 ltr:md:right-3 " onClick={() => setPasswordType(!passwordType)}>
                  {passwordType ? <Eye className="text-black dark:text-white" /> : <EyeSlash className="text-black dark:text-white" />}
                </span>
                <Input name="password" type={passwordType ? "password" : "text"} placeholder={t('password')} dir={lang === "ar" ? "rtl" : "ltr"} className="password" />
              </InputIcon>
              <div className="flex items-center justify-between -mt-4">
                <InputCheck name="remember" text={t('remember_me')} >
                </InputCheck>
                <Link href="/auth/forget-password">
                  <a className="flex flex-row-reverse -mt-2 [color:rgb(var(--primary-color))] " >{t('i_forgot_the_password?')}</a>
                </Link>
              </div>
              <ButtonTheme color="primary" as="button" type="submit" block size="md" className="my-6 text-center xs:my-4" loading={loadingButton}>
                {t('sign_in')}
              </ButtonTheme>
            </form>
          )}
        </Formik>
        <ButtonTheme color="primary" outline as="link" href="/auth/register-user" block size="xs" className="mt-6 text-center xs:my-4" >
          {t('you_do_not_have_an_account_create_an_account')}
        </ButtonTheme>
      </Login>
    </>
  )
}
LoginUser.getLayout = function PageLayout(page) {
  return <>
    {page}
  </>
}

