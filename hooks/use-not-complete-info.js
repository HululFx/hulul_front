import React, { useState, useContext, useEffect } from 'react'
import PermitionContext from "store/permition-context";
const useNotCompleteInfo = (type = true) => {
      const ctx = useContext(PermitionContext);
    const [notComplete, setNotComplete] = useState(true);
    useEffect(() => {
      console.log(ctx.userInfo)
        if (ctx.userInfo.financialProfile && ctx.userInfo.identity_confirmation   && ctx.userInfo.Address_confirmation && ctx.userInfo.email && ctx.userInfo.phone && ctx.userInfo.all_basicInfo) {
            setNotComplete(false)
        }
      }, [ctx])
    return notComplete;
  };
  export default useNotCompleteInfo;