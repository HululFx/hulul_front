import Trans from 'next-translate/Trans'
import FileSaver from "file-saver";
import XLSX from "xlsx";
import { Dropdown, Popover, Table, Whisper, InputPicker } from "rsuite";
import { Refresh2, SearchNormal1, ImportCurve, Calendar,More } from 'iconsax-react';
import { RecordCard, CustomDateRangePicker, ButtonTheme, Error, Loading } from "@/ui"
import useTranslation from 'next-translate/useTranslation'

const optionAction = [
    {
        "label": <Trans i18nKey="record:all" />,
        "value": "All",
    },
    {
        "label": <Trans i18nKey="record:sale" />,
        "value": "sale",
    },
    {
        "label": <Trans i18nKey="record:buy" />,
        "value": "buy",
    },

]

const ExportCSV = ({ csvData, fileName, Heading, wscols }) => {
  const { t, lang } = useTranslation("record")
    // ******** XLSX with object key as header *************
    // const fileType =
    //   "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    // const fileExtension = ".xlsx";

    // const exportToCSV = (csvData, fileName) => {
    //   const ws = XLSX.utils.json_to_sheet(csvData);
    //   const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
    //   const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
    //   const data = new Blob([excelBuffer], { type: fileType });
    //   FileSaver.saveAs(data, fileName + fileExtension);
    // };

    // ******** XLSX with new header *************
    const fileType =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const fileExtension = ".xlsx";



    const exportToCSV = (csvData, fileName, wscols, header) => {
        const ws = XLSX.utils.json_to_sheet(Heading, {
            header: header,
            skipHeader: true,
            origin: 0 //ok
        });
        ws["!cols"] = wscols;
        XLSX.utils.sheet_add_json(ws, csvData, {
            header: header,
            skipHeader: true,
            origin: -1 //ok
        });
        const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
        const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" });
        const data = new Blob([excelBuffer], { type: fileType });
        FileSaver.saveAs(data, fileName + fileExtension);
    };

    return (
        <span  onClick={e => exportToCSV(csvData, fileName, wscols)}>{t("excel")}</span>
    );
};

const wscolsOpenDeals = (lastLastData)=>[
    { wch: Math.max(...lastLastData.map(customer => customer.Login.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.Position.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.Action.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.Symbol.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.TimeCreate.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.PriceOpen.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.PriceCurrent.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.Profit.length)) }
];

// const HeadingOpenDeals = [
//     {
//         Position: <Trans i18nKey="record:operation_number" />,
//         Login: <Trans i18nKey="record:the_account" />,
//         Symbol: <Trans i18nKey="record:currency_type" />,
//         Action: <Trans i18nKey="record:the_operation" />,
//         TimeCreate: <Trans i18nKey="record:opening_time" />,
//         PriceOpen: <Trans i18nKey="record:the_opening_price" />,
//         PriceCurrent: <Trans i18nKey="record:price" />,
//         Profit: <Trans i18nKey="record:profit" />,

//     }
// ];
const headerOpenDeals = ["Login", "Position", "Action", "Symbol", "TimeCreate", "PriceOpen", "PriceCurrent", "Profit"]
const openDeals = {
    wscols: wscolsOpenDeals,
    // Heading: HeadingOpenDeals,
    header: headerOpenDeals,
}
const wscolsClodedDeals = (lastLastData)=>[
    { wch: Math.max(...lastLastData.map(customer => customer.Login.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.PositionID.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.Action.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.Symbol.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.Time.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.PricePosition.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.Price.length)) },
    { wch: Math.max(...lastLastData.map(customer => customer.Profit.length)) }
];

// const HeadingClodedDeals = [
//     {
//         Position: <Trans i18nKey="record:operation_number" />,
//         Login: <Trans i18nKey="record:the_account" />,
//         Symbol: <Trans i18nKey="record:currency_type" />,
//         Action: <Trans i18nKey="record:the_operation" />,
//         TimeCreate: <Trans i18nKey="record:opening_time" />,
//         PriceOpen: <Trans i18nKey="record:the_opening_price" />,
//         PriceCurrent: <Trans i18nKey="record:price" />,
//         Profit: <Trans i18nKey="record:profit" />,

//     }
// ];
const headerClodedDeals = ["Login", "PositionID", "Action", "Symbol", "TimeCreate", "PriceOpen", "PriceCurrent", "Profit"]
const closedDeals = {
    wscols: wscolsClodedDeals,
    // Heading: HeadingClodedDeals,
    header: headerClodedDeals,
}
const getData = (sortColumn = "", sortType = "",data) => {
    if (sortColumn && sortType) {
      return (data.sort((a, b) => {
        let x = a[sortColumn];
        let y = b[sortColumn];
        if (typeof x === 'string') {
          x = x.charCodeAt();
        }
        if (typeof y === 'string') {
          y = y.charCodeAt();
        }
        if (sortType === 'asc') {
          return x - y;
        } else {
          return y - x;
        }
      }
      ))
    }
    else {
      return data
    }
  };
  const iconPrint = (props, ref) => {
    return (
       <ButtonTheme {...props} ref={ref} color="primary" outline as="button" size="xs" className="p-4-important">
              <ImportCurve className="text-inherit w-[1.875rem] h-[1.875rem]"  />
          </ButtonTheme>
    );
  };
  var oneRem;

  const remTopx = (rem , size) => {
    if (size.width > 640) {
      oneRem = 16
    }
    if (size.width > 768) {
      oneRem = 16
    }
    if (size.width > 1024) {
      oneRem = 14
    }
    if (size.width > 1280) {
      oneRem = 14
    }
    if (size.width > 1400) {
      oneRem = 15
    }
    if (size.width > 1536) {
      oneRem = 17
    }
    return rem * oneRem
  }
export {
    optionAction, ExportCSV, openDeals,closedDeals,getData,iconPrint,remTopx
}