import React, { useState, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { MoneyRecive, ArrowLeft, CardSend ,UserOctagon,MoneySend} from 'iconsax-react';
import Link from "next/link"
import Head from 'next/head'
import {methods} from "@/container/deposit-and-draw/DepositDrawMoney"
import { Neteller, Skiller, MastrCard, USTD ,Visa} from "public/svg"
import {ButtonTheme , Loading} from "@/ui"
import useAuth from 'libs/useAuth'
import { useRouter } from 'next/router';
export default function DepositDrawChooseMethod({type}) {
    const router = useRouter()
  const {user, isLoading} = useAuth({middleware: 'auth'})
    const { t, lang } = useTranslation("depositAndDraw")
     if (isLoading || !user){
    return <Loading  page={true}/>
}
    return (
        <>
            <Head>
                <title>{type==="deposit" ? t("choose_the_deposit_method"):t("choose_the_draw_method")} | {t("common:website_name")} </title>
            </Head>
            <div className="p-3 bg-white rounded-lg lg:p-8 sm:p-4 dark:bg-dark-white md:rounded-xl">
                <div className="flex items-center justify-between mb-6">
                    <div className="flex items-center gap-2 ">
                        <div className=" icon-container">
                        {type==="deposit" ? <MoneyRecive className="[color:rgb(var(--primary-color))] lg:w-8 w-4 lg:h-8 h-4" /> :<MoneySend size="25" className="[color:rgb(var(--primary-color))]" />}
                        </div>
                        <h1 className="block text-lg font-bold text-black lg:text-3xl dark:text-white">{type==="deposit" ? t("choose_the_deposit_method"):t("choose_the_draw_method")}</h1>
                    </div>
                    <Link href={`/dashboard/${type}`} >
                        <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl">
                            <ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
                        </a>
                    </Link>
                </div>
                <div className="py-4 ">
                    {methods(type).map((method, index) => (
                        <div key={index} className="mb-8">
                            <h2 className="text-gray-400">{method.title}</h2>
                            <ul className="grid grid-cols-2 gap-4 lg:grid-cols-7 lg:gap-10">
                            {method.list.map((m, i) => (
                                <li key={i} className="col-span-2">
                                    <button onClick={()=>router.push(
                                        {pathname: m.href,
                                            query: { account: router.query.account },
                                        })} className="flex items-center w-full gap-2 px-8 py-5 rounded-xl bg-secondary dark:bg-dark-secondary dark:hover:bg-hover-dark-secondary hover:bg-hover-secondary">
                                    {m.icon}{m.title}
                                    </button>
                                </li>
                            ))}
                            </ul>
                        </div>
                    ))}
                </div>
            </div>
        </>
    )
}
