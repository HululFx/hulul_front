import React, { useState, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import Link from "next/link"
import { ButtonTheme, CopyToClip, Slider, Error, Loading, NoData, Slider2 } from "@/ui"
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { Lock, Eye, EyeSlash, EmptyWallet, Cards, MoneyRecive, MoneySend, ArrowLeft, CardSend, UserOctagon } from 'iconsax-react';
import { Input, InputIcon, CustumnCheckbox, SelectWIthHead, CustomnCheckColors } from "@/form"
import { Trider4, Correct } from "public/svg"
import { useRouter } from 'next/router';
import { WaitModal, WarningModal } from "@/modals"
import Head from 'next/head'
import useNotCompleteInfo from "hooks/use-not-complete-info"
import { changeRealAccountSetting, userRealAccountWithoutPagination } from "apiHandle"
import useSWR from 'swr'
import { Neteller, Skiller, MastrCard, USTD, Visa } from "public/svg"
import Trans from 'next-translate/Trans'
import useAuth from 'libs/useAuth'
export default function DepositDrawMoney({ type }) {
    const { user, isLoading } = useAuth({ middleware: 'auth' })
    const router = useRouter;
    const { t, lang } = useTranslation("depositAndDraw")
    const [currentStep, setCurrentStep] = useState(0)
    const { data, error } = useSWR(userRealAccountWithoutPagination())
    const [currentAccount, setCurrentAccount] = useState()
    const [currentId, setCurrentId] = useState()

    const [allAccounts, setAllAccounts] = useState()
    useEffect(() => {
        if (data && data.demo_accounts_Informations.length) {
            setCurrentId(data.demo_accounts_Informations[0].id)
            setCurrentAccount(data.demo_accounts_Informations[0])
            setAllAccounts(data.demo_accounts_Informations)
        }
    }, [data])
    const handleChooseIndexSlide = (i) => {
        // router.push({
        //   pathname: router.asPath.split("?")[0],
        //   query: { account: data.demo_accounts_Informations[i].id },

        // }, undefined, { scroll: false })
        setCurrentAccount(data.demo_accounts_Informations[i])
        setCurrentId(data.demo_accounts_Informations[i].id)
    }
    if (isLoading || !user){
        return <Loading  page={true}/>
    }
    return (
        <>
            <Head>
                <title>{type === "deposit" ? t("deposit_amount") : t("draw_amount")} | {t("common:website_name")} </title>
            </Head>
            <div className="p-3 bg-white rounded-lg lg:p-8 sm:p-4 dark:bg-dark-white md:rounded-xl">
                <div className="flex items-center justify-between mb-6">
                    <div className="flex items-center gap-2 ">
                        <div className=" icon-container">
                            {type === "deposit" ? <MoneyRecive className="w-4 h-4 [color:rgb(var(--primary-color))]  lg:w-8 lg:h-8" /> : <MoneySend className="w-4 h-4 [color:rgb(var(--primary-color))]  lg:w-8 lg:h-8" />}
                        </div>
                        <h1 className="block text-lg font-bold text-black lg:text-3xl dark:text-white">{type === "deposit" ? t("deposit_amount") : t("draw_amount")}</h1>
                    </div>
                    {currentStep == 0 ?
                        <Link href="/dashboard" >
                            <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl">
                                <ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
                            </a>
                        </Link>
                        :
                        <button onClick={() => setCurrentStep(0)} className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl" ><ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
                        </button>
                    }
                </div>
                <div className="py-4 ">
                    {currentStep == 0 ?
                        <div className="py-14 ">
                            <h2 className="mb-8 text-lg text-center lg:text-2xl font-weight">{type === "deposit" ? t("what_place_do_you_want_to_deposit_the_amount") : t("what_place_do_you_want_to_draw_the_amount")}</h2>
                            <div className="flex flex-col items-center justify-center gap-10 lg:flex-row">
                                <Link href={`/dashboard/${type}/choose-method`}>
                                    <a className="p-6 text-center rounded bg-secondary dark:bg-dark-secondary w-[12rem]">
                                        <EmptyWallet className="[color:rgb(var(--primary-color))] block mx-auto lg:w-[5.625rem] lg:h-[5.625rem] w-[4.375rem] h-[4.375rem]" />
                                        <h3>{t("for_my_wallet")}</h3>
                                    </a>
                                </Link>
                                <button onClick={() => setCurrentStep(1)} className="p-6 text-center rounded bg-secondary dark:bg-dark-secondary w-[12rem]">
                                    <Cards className="[color:rgb(var(--primary-color))] block mx-auto lg:w-[5.625rem] lg:h-[5.625rem] w-[4.375rem] h-[4.375rem]" />
                                    <h3>{t("real_accounts")}</h3>
                                </button>
                            </div>
                        </div>
                        :
                        <div className="py-4 w-[50rem] max-w-full mx-auto">
                            {error ? <Error apiMessage={error} />
                                : (!data || !currentAccount) ? <Loading />
                                    : <div className="max-w-full py-4 mx-auto ">
                                        <h2 className="mb-8 text-lg text-center lg:text-2xl font-weight">{type === "deposit" ? t("choose_the_real_deposit_account") : t("choose_the_real_draw_account")}</h2>
                                        <div className="mb-14 ">
                                            <Slider2 data={allAccounts} currentAccount={currentAccount} type="demo" chooseSlide={allAccounts.indexOf(currentAccount)} handleChooseIndexSlide={handleChooseIndexSlide} />
                                        </div>
                                        <div className=" w-[37.5rem] max-w-full rtl:ml-auto ltr:mr-auto">
                                            <ButtonTheme color="primary" block size="xs" className="flex items-center justify-center gap-2 " as="link" href={`/dashboard/${type}/choose-method?account=${currentId}`}>{t('complete_the_process')}</ButtonTheme>
                                        </div>
                                    </div>
                            }
                        </div>
                    }
                </div>
            </div>
        </>
    )
}

export function methods(type) {
    return [
        {
            title: <Trans i18nKey="depositAndDraw:electronic_portfolio" />,
            list: [
                { title: "Neteller", icon: <Neteller />, href: `/dashboard/${type}/money/electronic-portfolio/neteller` },
                { title: "skrill", icon: <Skiller />, href: `/dashboard/${type}/money/electronic-portfolio/skrill` },
            ],
            collapse: `/dashboard/${type}/money/electronic-portfolio`
        },
        {
            title: <Trans i18nKey="depositAndDraw:credit_card" />,
            list: [
                { title: "visa", icon: <Visa />, href: `/dashboard/${type}/money/credit-card/visa` },
                { title: "mastr card", icon: <MastrCard />, href: `/dashboard/${type}/money/credit-card/master-card` },
                { title: <Trans i18nKey="depositAndDraw:bank_transfer" />, icon: <div className="icon-container"><CardSend className="[color:rgb(var(--primary-color))] " size="20" /></div>, href: `/dashboard/${type}/money/credit-card/bank-transfer` },
            ],
            collapse: `/dashboard/${type}/money/credit-card`
        },
        {
            title: <Trans i18nKey="depositAndDraw:cross_currencies" />,
            list: [
                { title: "USTD", icon: <USTD />, href: "/#" }
            ],
            collapse: `/dashboard/${type}/money/cross-currencies`
        },
        {
            title: <Trans i18nKey="depositAndDraw:another_way" />,
            list: [
                { title: <Trans i18nKey="depositAndDraw:certified_broker" />, icon: <div className="icon-container"><UserOctagon className="[color:rgb(var(--primary-color))] " size="20" /></div>, href: `/dashboard/${type}/money/other/broker` }
            ],
            collapse: `/dashboard/${type}/money/other`
        },
    ]
}