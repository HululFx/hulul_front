import React, { useState , useContext ,useEffect} from 'react'
import useTranslation from 'next-translate/useTranslation'
import Link from "next/link"
import { Progress ,Badge} from 'rsuite';
import { useRouter } from 'next/router'
import { SmallAside } from "@/ui"
import { Location, ClipboardText, Lock1, Call, ProfileTick, Profile, LocationTick ,ReceiptEdit,MoneySend ,MoneyRecive ,ArrowLeft} from 'iconsax-react'
import PermitionContext from "store/permition-context";
import {methods} from "@/container/deposit-and-draw/DepositDrawMoney"

export default function DepositDrawContainer({ ...props }) {
  const { t, lang } = useTranslation("depositAndDraw")
  const router = useRouter()
 
  return (
    <>
      <div className="p-4 bg-white rounded-t-lg lg:p-8 dark:bg-dark-white md:rounded-t-xl">
      <div className="flex items-center justify-between mb-6">
                    <div className="flex items-center gap-2 ">
                        <div className=" icon-container">
                            {props.type === "deposit" ? <MoneyRecive className="[color:rgb(var(--primary-color))]  lg:w-8 w-4 lg:h-8 h-4" /> :<MoneySend className="[color:rgb(var(--primary-color))]  lg:w-8 w-4 lg:h-8 h-4" /> }
                        </div>
                        <h1 className="block text-lg font-bold text-black lg:text-3xl dark:text-white">{props.type === "deposit"?t("deposit"):t("draw")}</h1>
                    </div>
                    <Link href={`/dashboard/${props.type}/choose-method`} >
                        <a className="p-2 border [border-color:rgba(var(--primary-color),1)] rounded-xl">
                            <ArrowLeft size="25" className={`[color:rgb(var(--primary-color))]${lang === "ar" ? "" : "transform rotate-180"}`} />
                        </a>
                    </Link>
                </div>
       
      </div>
      <div className="bg-white dark:bg-dark-white ">
        <div className={`${"lg:grid grid-cols-11 h-full min-h-[calc(100vh_-_250px)]"}`}>
          <SmallAside type="small" collapse={true} roots={methods(props.type)} className="border-gray-300 xl:col-span-3 lg:col-span-4 rtl:border-l ltr:border-r rtl:pl-4 ltr:pr-4" steps={5}/>
          <div className={`${"xl:col-span-8 lg:col-span-7"} lg:px-8 px-3`}>
            {props.children}
          </div>
        </div>
      </div>
    </>
  )
}
