import React, { useState, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import  DepositDrawContainer  from "@/container/deposit-and-draw/DepositDrawContainer"
import {  Gift } from 'iconsax-react'
import { Input } from '@/form'
import { AddNewBankModal } from '@/modals'
import { Formik } from "formik";
import { ButtonTheme, Error, Loading, CreateBankAccount, CardAccountVisa } from "@/ui"
import { CheckUserData, profilePersonalIdentificationConfirmation, profileAddressCheck } from "apiHandle"
import Head from 'next/head'
import useSWR, { useSWRConfig } from 'swr'
import {  Navigation } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import useAuth from 'libs/useAuth'
export default function CreditCardDepositDrawId({type}) {
  const {user, isLoading} = useAuth({middleware: 'auth'})
  const { t, lang } = useTranslation("depositAndDraw")
  const [choose, setChoose] = useState(-1)
  const [done, setDone] = useState(false)
  const [change, setChange] = useState(false)
  const [chooseValue, setChooseValue] = useState()
  const [loadingButton, setLoadingButton] = useState(false)
  const [openAddAccount, setOpenAddAccount] = useState(false)
  const { mutate } = useSWRConfig();

  const onSubmit = (values) => {
    setLoadingButton(true);
    profilePersonalIdentificationConfirmation({
      values: { ...values, document_type: chooseValue },
      success: () => { setLoadingButton(false); setDone(true); mutate(CheckUserData()) },
      error: () => setLoadingButton(false)
    })
  }
 if (isLoading || !user){
    return <Loading  page={true}/>
}
  return (
    <>
      <Head>
        <title>{type==="deposit"? t("deposit_from_payment_card"):t("draw_from_payment_card")} | {t("common:website_name")}</title>
      </Head>
      <DepositDrawContainer type={type}>
        <h2 className="text-xl text-gray-600 mb-4">{t("payment_card")}</h2>
        <div className="grid xl:grid-cols-2 grid-cols-1 gap-10 mb-20">
        <Swiper
          dir={lang === 'ar' ? "rtl" : "ltr"}
          loop="true"
          slidesPerView={1}
          navigation={true}
          spaceBetween={10}
          // onSlideChange={(e) => {handleCLickSlider(e.isEnd ? 0 : e.snapIndex - 2);}}
          modules={[ Navigation]}
          className="mySwiper bank-slider w-full"
        >
         {Array.from({ length: Number.parseInt(10) }, (item, i) => (
            <SwiperSlide key={i}>
              <CardAccountVisa color="bg-[#505BBD]" />
            </SwiperSlide>
          ))}
        </Swiper>
          <CreateBankAccount text={t("common:add_a_new_card")} onClick={()=>setOpenAddAccount(true)} />
        </div>
        <h3 className="text-base text-gray-600 mb-2">{type==="deposit"?  t("enter_the_deposit_amount") : t("enter_the_draw_amount")}</h3>
        <Formik
          enableReinitialize
          initialValues={{ deposit: "2000", gift: "" }}
          onSubmit={onSubmit} >
          {(props) => {
            props.dirty && setChange(true)
            return (
              <form onSubmit={props.handleSubmit}>
                <div className="flex bg-secondary dark:bg-dark-secondary rounded-xl py-1 ltr:pr-1 rtl:pl-1 mb-4">
                  <Input name="deposit" type="text" dir={lang === "ar" ? "rtl" : "ltr"} noMarginBottom className="grow relative mr-2" />
                  <span className="bg-white dark:bg-dark-white text-gray-400 grid lg:grid-cols-2 grid-cols-1 text-center items-center justify-center px-6 rtl:rounded-l-xl ltr:rounded-r-xl "><span className="whitespace-nowrap	">{t("you_will_get")}</span> <bdi>{props.values.deposit - 20}$</bdi></span>
                </div>
                <bdi className="flex gap-2 text-xs items-center mb-4">
                  <Gift className="[color:rgb(var(--primary-color))] " size="20" />
                  <span className=" font-bold text-[0.875rem]">$100</span>
                  <span>+</span>
                  <span>%10 {t("gift")}</span>
                </bdi>
                <Input name="gift" type="text" dir={lang === "ar" ? "rtl" : "ltr"} placeholder={t("add_a_gift_coupon")} border />

                <ButtonTheme color="primary" as="button" type="submit" size="md" block className="my-8 text-center" loading={loadingButton} disabled={!change} >
                  {t('pay_now')}
                </ButtonTheme>
              </form>
            )
          }}
        </Formik>
      </DepositDrawContainer >
      <AddNewBankModal open={openAddAccount} onClose={()=>setOpenAddAccount(false)}/>
    </>
  )
}
