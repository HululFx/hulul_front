import React, { useState, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import  DepositDrawContainer  from "@/container/deposit-and-draw/DepositDrawContainer"
import { Gift } from 'iconsax-react'
import { Input, InputShow, UploadDraggableImage } from '@/form'
import { AddNewBankModal } from '@/modals'
import { Formik } from "formik";
import { ButtonTheme, Error, Loading, } from "@/ui"
import { CheckUserData, profilePersonalIdentificationConfirmation, profileAddressCheck } from "apiHandle"
import Head from 'next/head'
import useSWR, { useSWRConfig } from 'swr'
import { NetellerText ,SkillerText} from "public/svg"
import useAuth from 'libs/useAuth'
export default function ElectronicDepositDraw({type, electronic}) {
  const {user, isLoading} = useAuth({middleware: 'auth'})
    const { t, lang } = useTranslation("depositAndDraw")
    const [choose, setChoose] = useState(-1)
    const [change, setChange] = useState(false)
    const [chooseValue, setChooseValue] = useState()
    const [loadingButton, setLoadingButton] = useState(false)
    const [openAddAccount, setOpenAddAccount] = useState(false)
    const { mutate } = useSWRConfig();
 if (isLoading || !user){
    return <Loading  page={true}/>
}
    const onSubmit = (values) => {
        setLoadingButton(true);
        // profilePersonalIdentificationConfirmation({
        //     values: { ...values, document_type: chooseValue },
        //     success: () => { setLoadingButton(false); setDone(true); mutate(CheckUserData()) },
        //     error: () => setLoadingButton(false)
        // })
    }

    return (
        <>
            <Head>
                <title>{type==="deposit"? t("deposit_from_the_electronic_wallet_electronic", { electronic: electronic }) :t("draw_from_the_electronic_wallet_electronic", { electronic: electronic })} | {t("common:website_name")}</title>
            </Head>
            <DepositDrawContainer type={type}>
            <div className="h-full flex items-center">
                <Formik
                    enableReinitialize
                    initialValues={{ draw: "2000", gift: "" }}
                    onSubmit={onSubmit} >
                    {(props) => {
                        props.dirty && setChange(true)
                        return (
                            <form onSubmit={props.handleSubmit} className="w-full">
                                <h3 className="text-base text-gray-600 mb-2">{t("enter_the_draw_amount")}</h3>
                                <div className="flex bg-secondary dark:bg-dark-secondary rounded-xl py-1 ltr:pr-1 rtl:pl-1 mb-4">
                                    <Input name="draw" type="text" dir={lang === "ar" ? "rtl" : "ltr"} noMarginBottom className="grow relative mr-2" />
                                    <span className="bg-white dark:bg-dark-white text-gray-400 grid lg:grid-cols-2 grid-cols-1 lg:gap-8 text-center items-center justify-center px-6 rtl:rounded-l-xl ltr:rounded-r-xl "><span className="whitespace-nowrap">{t("you_will_get")}</span> <bdi>{props.values.draw - 20}$</bdi></span>
                                </div>
                                <bdi className="flex gap-2 text-xs items-center mb-4">
                                    <Gift className="[color:rgb(var(--primary-color))] " size="20" />
                                    <span className=" font-bold text-[0.875rem]">$100</span>
                                    <span>+</span>
                                    <span>%10 {t("gift")}</span>
                                </bdi>
                                <ButtonTheme color={electronic==="skrill"? "bg-[#83BA3B]" : "bg-[#5E1E5D]" } as="button" type="submit" size="md" block className="my-8 text-center" loading={loadingButton} disabled={!change} >
                                    <div className="flex justify-center items-center gap-2">
                                       <span>{t('pay_now')}</span> 
                                       <span className="hidden lg:block">
                                    {electronic==="skrill"? <SkillerText width="100"/> :<NetellerText width="100"/>}

                                       </span>
                                    </div>
                                </ButtonTheme>
                            </form>
                        )
                    }}
                </Formik>
                </div>
            </DepositDrawContainer >
        </>
    )
}
