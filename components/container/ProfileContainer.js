import React, { useState, useContext, useEffect } from 'react'
import useTranslation from 'next-translate/useTranslation'
import Link from "next/link"
import { Progress, Badge } from 'rsuite';
import { useRouter } from 'next/router'
import { SmallAside } from "@/ui"
import { Location, ClipboardText, Lock1, Call, ProfileTick, Profile, LocationTick, ReceiptEdit, Cards } from 'iconsax-react'
import PermitionContext from "store/permition-context";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import useWindowSize from "hooks/use-window";

export default function ProfileContainer({ tab, ...props }) {
  const size = useWindowSize();
  const ctx = useContext(PermitionContext);
  const { t, lang } = useTranslation("profile")
  const [percent, setPercent] = useState("0");
  const [initialSlide, setInitialSlide] = useState(0)
  useEffect(() => {
    var p = 0;
    if (ctx.userInfo.financialProfile) {
      p += 25;
    }
    if (ctx.userInfo.Address_confirmation) {
      p += 12.5;
    }
    if (ctx.userInfo.identity_confirmation) {
      p += 12.5;
    }
    if (ctx.userInfo.all_basicInfo) {
      p += 25;
    }
    setPercent(p)
  }, [ctx])

  const router = useRouter()
  const navs = [
    { title: t("personal_information"), href: "/dashboard/profile/personal/profile-personally", star: ctx.userInfo.all_basicInfo },
    { title: t("financial_information"), href: "/dashboard/profile/financial-information", star: ctx.userInfo.financialProfile },
    { title: t("upload_documents"), href: "/dashboard/profile/upload-documents/identification-confirmation", star: (ctx.userInfo.identity_confirmation && ctx.userInfo.Address_confirmation ) },
    { title: t("bank_account_and_cards"), href: "/dashboard/profile/bank-account/details-of-the-bank-account", star: true },
  ]
  const roots = {
    personal: [
      { title: t('profile_personly'), icon: <ClipboardText size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard/profile/personal/profile-personally", },
      { title: t('contact_information'), icon: <Lock1 size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard/profile/personal/contact-information", },
      { title: t('headlines'), icon: <Call size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard/profile/personal/headlines", },
      { title: t('safety_and_password'), icon: <Location size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard/profile/personal/safety-and-password", },
    ],
    uploadDocuments: [
      { title: t('identification_confirmation'), icon: <ProfileTick size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard/profile/upload-documents/identification-confirmation", },
      { title: t('confirm_the_address'), icon: <LocationTick size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard/profile/upload-documents/confirm-the-address", },
    ],
    bankAccount: [
      { title: t('details_of_the_bank_account'), icon: <ReceiptEdit size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard/profile/bank-account/details-of-the-bank-account", },
      { title: t('payment_cards'), icon: <Cards size="18" className="[color:rgb(var(--primary-color))] " />, href: "/dashboard/profile/bank-account/payment_cards", },
    ]
  }
  useEffect(() => {
    for (var i = 0; i < navs.length; i++) {
      if (router.pathname.startsWith(navs[i].href.split("/").splice(0, 4).join("/"))) {
        setInitialSlide(i)
      }

    }
  }, [router,initialSlide])
  return (
    <>
      <div className="bg-white rounded-t-lg lg:pb-8 dark:bg-dark-white md:rounded-t-xl">
        <div className="flex flex-col items-center justify-between p-4 mb-8 lg:p-8 lg:flex-row">
          <div className="flex items-center gap-2 mb-12 lg:mb-6 rtl:ml-auto ltr:mr-auto">
            <div className=" icon-container">
              <Profile className="[color:rgb(var(--primary-color))] -400 lg:w-8 w-4 lg:h-8 h-4" />
            </div>
            <h1 className="block text-lg font-bold text-black lg:text-3xl dark:text-white">{t("profile_personly")}</h1>
          </div>
          <div className="max-w-full mx-auto w-120">
            <span className="block mx-4 -my-2 text-xs text-gray-400">{percent < 100 ? t("your_account_information_is_not_completed") : t("your_account_information_is_completed")}</span>
            <Progress.Line percent={percent} status={percent < 100 ? "active" : "success"} strokeColor={percent < 100 ? "rgb(var(--primary-color))" : "var(--success-color)"} trailColor="var(--secondary-color)" />
          </div>
        </div>
        <div className="lg:mx-8 lg:mt-8 p-[0.375rem] relative lg:flex justify-between mb-0 lg:bg-secondary lg:dark:bg-dark-secondary   rounded-xl">
          {size.width > process.env.lg ?
            navs.map((nav, index) => (
              <Link key={index} href={nav.href}  ><a className={` rounded-xl lg:py-5 py-2  z-1 text-center lg:text-lg text-sm lg:w-1/4 w-max ${router.pathname.startsWith(nav.href.split("/").splice(0, 4).join("/")) ? "text-black dark:text-white bg-white dark:bg-dark-white" : "text-gray-400"}`}>{nav.title}{+nav.star != 1 && <span className="text-3xl text-danger leading-0">*</span>}</a></Link>
            ))
            :
            <Swiper
              dir={lang === 'ar' ? "rtl" : "ltr"}
              navigation={true}
              modules={[Navigation]}
              className="mySwiper profile-slider"
              slidesPerView={"auto"}
             
              initialSlide={router.pathname.startsWith(navs[0].href.split("/").splice(0, 4).join("/")) ? 0 : router.pathname.startsWith(navs[1].href.split("/").splice(0, 4).join("/")) ? 1 : router.pathname.startsWith(navs[2].href.split("/").splice(0, 4).join("/")) ? 2 : router.pathname.startsWith(navs[3].href.split("/").splice(0, 4).join("/")) ? 3 :0}
             
              runCallbacksOnInit
            >
              {navs.map((nav, i) => (
                <SwiperSlide key={i} className=' w-max'>
                  <Link href={nav.href}  ><a className={` rounded-xl  lg:py-5 p-1  z-1 text-center lg:text-lg text-sm lg:w-1/4  ${router.pathname.startsWith(nav.href.split("/").splice(0, 4).join("/")) ? "text-black dark:text-white whitespace-nowrap	 bg-white dark:bg-dark-white" : "text-gray-400"}`}>{nav.title}{+nav.star != 1 && <span className="text-3xl text-danger leading-0">*</span>}</a></Link>
                </SwiperSlide>
              ))}
            </Swiper>
          }
        </div>
      </div>
      <div className="bg-white dark:bg-dark-white ">
        <div className={`${tab && "lg:grid grid-cols-11 "} h-full md:min-h-[calc(100vh_-_250px)] min-h-[calc(100vh_-_500px)]`}>
          {tab && <SmallAside type="small" roots={roots[`${tab}`]} className="col-span-3 border-gray-300 rtl:border-l ltr:border-r rtl:pl-4 ltr:pr-4" />}
          <div className={`${tab ? "col-span-8" : ""} lg:p-8 p-4  min-h-inherit overflow-hidden`}>
            {props.children}
          </div>
        </div>
      </div>
    </>
  )
}
