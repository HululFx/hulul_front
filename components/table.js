
import React, { useState, useRef ,useEffect} from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Refresh2, SearchNormal1, ImportCurve, Calendar, More } from 'iconsax-react';
import { Dropdown, Popover, Table, Whisper, InputPicker } from "rsuite";
import { RecordCard, CustomDateRangePicker, ButtonTheme, Error, Loading } from "@/ui"
import { useRouter } from 'next/router'

function AccountCell({ rowData, dataKey, color, accountInfo, ...props }) {
    const { t, lang } = useTranslation("common")
    return (
        <Table.Cell {...props}>
            <div className={`flex items-center h-full ${lang === "ar" && "justify-end"} `}>
                <div className="p-3 px-6 text-white rounded-md bg-color w-max" style={{ "--color": accountInfo.filter(x => +x.login === +rowData[dataKey])[0].color }}>
                    {rowData[dataKey]}
                </div>
            </div>
        </Table.Cell>

    )
}

function ProfitCell({ rowData, dataKey, ...props }) {
    const { t, lang } = useTranslation("common")
    return (
        <Table.Cell {...props}>
            <bdi className={`${rowData[dataKey] > 0 ? "text-success" : "text-danger"} flex items-center h-full ${lang === "ar" && "justify-end"} `}>
                <div className={`${lang === "ar" ? "text-right" : "text-left"} w-full`}>{rowData[dataKey] > "0" ? " + " : rowData[dataKey] === "0" ? "" : " - "}{Math.abs(rowData[dataKey])}</div>
            </bdi>
        </Table.Cell>
    )
}
function CustomCell({ rowData, dataKey, ...props }) {
    const { t, lang } = useTranslation("common")
    return (
        <Table.Cell {...props}>
            <bdi className={`${props.className} flex items-center h-full ${lang === "ar" && "justify-end"}`}>
                {rowData[dataKey]}
            </bdi>
        </Table.Cell>
    )
}
function CellPriceAndTime({ rowData, dataKey, ...props }) {
    const { t, lang } = useTranslation("common")
    return (
        <Table.Cell {...props}>
            <bdi className={`${props.className} flex items-end flex-col h-full ${lang === "ar" && "justify-end"}`}>
                <div>
                {rowData[dataKey]}
                </div>
                    <div>
                    {new Date(rowData['Time'] * 1000).toLocaleTimeString(['en-US'], {
                    year: "numeric",
                    month: "2-digit",
                    day: "2-digit",
                    hour: "2-digit",
                    minute: "2-digit"
                })}
                    </div>
            </bdi>
        </Table.Cell>
    )
}
function TimeCell({ rowData, dataKey, ...props }) {
    const { t, lang } = useTranslation("common")
    return (

        <Table.Cell {...props}>
            <bdi className={`${props.className} flex items-center h-full ${lang === "ar" && "justify-end"}`}>
                {/* //! change local each language */}
                {new Date(rowData[dataKey] * 1000).toLocaleTimeString(['en-US'], {
                    year: "numeric",
                    month: "2-digit",
                    day: "2-digit",
                    hour: "2-digit",
                    minute: "2-digit"
                })}
            </bdi>
        </Table.Cell>
    )
}
function OperationNumberCell({ rowData, dataKey, ...props }) {
    const { t, lang } = useTranslation("record")
    return (
        <Table.Cell {...props}>
            <bdi className={`${props.className} flex items-center h-full ${lang === "ar" && "justify-end"}`}>
                #{rowData[dataKey]}
            </bdi>
        </Table.Cell>
    )
}

function TypeCell({ rowData, dataKey, ...props }) {
    const { t, lang } = useTranslation("record")

    return (
        <Table.Cell {...props}>
            <bdi className={`${props.className} flex items-center h-full `}>
                {rowData[dataKey] === 0 ? t("sale") : t("buy")}
            </bdi>
        </Table.Cell>
    )
}
function PriceCell({ rowData, dataKey, ...props }) {
    const { t, lang } = useTranslation("record")
    return (
        <Table.Cell {...props}>
            <bdi className={`${props.className} flex  h-full  justify-center flex-col	`}>
                <div className={`mb-1 text-right font-bold ${lang === "ar" ? "text-right" : "text-left"}`}>{rowData[dataKey]}</div>
                <div className={`text-gray-400 text-md ${lang === "ar" ? "text-right" : "text-left"}`}>2018/2/2 - 12:50am</div>
            </bdi>
        </Table.Cell>
    )
}
function ActionColumn({ label, onfilter, column, ...props }) {

    const filtering = "";
    const router = useRouter()
    const [sorting, setSorting] = useState("asc")
    const filterElement = useRef();
    const { t, lang } = useTranslation("record")
    const [searchState, setSearchState] = useState(column != "action" ? router.query.login && router.query.login : "All")
    useEffect(()=>{
        setSearchState( router.query.login ? router.query.login : "All")
    },[router])
    const renderMenu = ({ onClose, left, top, className }, ref) => {
        const closeDrop = (type = sorting, filter = "") => {
            onClose();
            onfilter(type, props.dataKey, filter);
            if (column != "action" && !type) {
                router.push({ query: { login: filter } }, undefined, { scroll: false })
            }
            // history.push(`${location.pathname}? column = ${props.dataKey}${`&sort=${type}`}${filterElement.current.value && `&search=${filterElement.current.value}`}`);
        }

        return (
            <Popover ref={ref} className={className} style={{ left, top }} full>
                <Dropdown.Menu >
                    <Dropdown.Item className="px-4" active={sorting === "asc"} onClick={() => { sorting === "asc" ? closeDrop(null) : closeDrop("asc"); setSorting("asc") }}>{t("ascending_order")}</Dropdown.Item>
                    <Dropdown.Item className="px-4" active={sorting === "desc"} onClick={() => { sorting === "desc" ? closeDrop(null) : closeDrop("desc"); setSorting("desc") }}>{t("descending_order")}</Dropdown.Item>
                    <Dropdown.Item active={filtering}>
                        <InputPicker data={props.options} style={{ width: 224 }} ref={filterElement} cleanable={false} searchable={column != "action"}
                        className="table-picker"
                            onChange={(value) => { setSearchState(value); closeDrop(null, value); }}
                            locale={{ noResultsText: t("common:there_are_no_results"), placeholder: t("common:choose"), searchPlaceholder:t("common:search") }}
                            defaultValue={searchState} />
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Popover>
        );
    };
    return (
        <Table.HeaderCell {...props}>
            <Whisper placement="autoVerticalStart" trigger="click" speaker={renderMenu}>
                <button className="flex items-baseline gap-4" >
                    <span className="relative -top-2 ">
                        <bdi >
                            <span>{label}</span>
                            {column != "action" && <span className="w-8 h-4 bg-color inline-block rounded mx-2 " style={{ "--color": searchState == "All" ? "#eee" : searchState && props.accountInfo.filter(x => +x.login === +searchState)[0].color }}></span>}
                            <span className="dark:text-white text-black">{" "}{column != "action" ? searchState : searchState != "All" && t(searchState)}{" "}</span>
                        </bdi>
                    </span>
                    <More className="transform rotate-90 w-4 rs-table-cell-header-icon-sort" width="10" />
                </button>
            </Whisper>
        </Table.HeaderCell>
    )
}
export {
    AccountCell,
    ProfitCell,
    CustomCell,
    TimeCell,
    OperationNumberCell,
    TypeCell,
    PriceCell,
    ActionColumn,
    CellPriceAndTime

}