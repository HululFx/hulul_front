import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import { NoWifi } from "public/svg"
import {Warning2} from 'iconsax-react';

export default function Error({ message, apiMessage }) {
  const type = message ? message :apiMessage ? apiMessage.message ==="Network Error" && "network" : "other"
  const { t, lang } = useTranslation("common")
  return (
    type === "network" ?
      <div className="text-center md:py-20 pb-10 col-span-2">
        <NoWifi className="text-white dark:text-dark-white  mb-4 mx-auto md:w-[9.375rem] w-[6.25rem]" />
        <h2 className="text-black dark:text-white font-bold lg:text-3xl text-xl mb-2">{t("there_is_no_internet_connection")}</h2>
        <p className="text-lg text-gray-400">{t("try_to_make_sure_the_modem_connects_or_that_your_device_is_connected_to_the_internet")}</p>
      </div>
      :
      <div className="text-center py-20 col-span-2">
        <Warning2  size="150" className="dark:text-white text-black  mb-4 mx-auto md:w-[9.375rem] w-[6.25rem]" />
        <h2 className="text-black dark:text-white font-bold lg:text-3xl text-xl mb-2">{t("sorry_a_problem_occurred_please_return_laterrn_later")}</h2>
     </div>
        )
}

    // {/* <NoWifi className="text-white dark:text-dark-white" />
    // <h2>{t("there_is_no_internet_connection")}</h2>
    // <div>{}</div> */}
    // </>