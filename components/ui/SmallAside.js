import React, { useState, useEffect } from 'react'
import Link from "next/link"
import { useRouter } from 'next/router'
import { DropdownButton } from "@/ui"
import useWindowSize from "hooks/use-window";
import { SelectPicker } from 'rsuite';
import useTranslation from 'next-translate/useTranslation'
export default function SmallAside({ roots, type, steps, ...props }) {
  const { t, lang } = useTranslation("depositAndDraw")
  const router = useRouter()
  const changeRoute = (values) => {
    router.push({
      pathname: values,
      query: (router.query.account && type != "main") && { account: router.query.account },
    })
  }
  const changeCollapsRoots = (roots) => {
    var newRoots = []
    var counter = 0;
    for (var i = 0; i < roots.length; i++) {
      newRoots.push(...roots[i].list);
      for (var j = 0; j < roots[i].list.length; j++) {
        newRoots[counter].group = t(roots[i].title.props.i18nKey)
        counter++;
      }
    }
    
    return newRoots
  }
  const size = useWindowSize();
  const conditionCollapse = (root) => {
    return router.pathname.split("/").splice(0, steps).join("/") == root.split("/").splice(0, steps).join("/")
  }

  // useEffect(() => {
  //   setOpenCollapse(false)
  // }, [router])
  return (
    (size.width > process.env.lg || type === 'main') ?
      <ul className={`p-0 m-0 ${props.className}`}>
        {roots.map((root, index) => (
          <li key={index}>
            {root.href ? <Link href={root.href} >
              <a className={`flex items-center gap-4  mb-2 py-3  ltr:pl-8  rtl:pr-8   hover:bg-secondary/80 dark:hover:bg-dark-secondary  ${type === "main" ? conditionCollapse(root.href) && 'rtl:border-r-8 ltr:border-l-8 [border-color:rgba(var(--primary-color),1)] bg-secondary dark:bg-dark-secondary' : router.pathname === root.href && 'rtl:border-r-8 ltr:border-l-8 [border-color:rgba(var(--primary-color),1)] bg-secondary dark:bg-dark-secondary'}`}>
                <div className="icon-container">
                  {root.icon}
                </div>
                <span className="text-base text-black dark:text-white" >
                  {root.title}
                </span>
              </a>
            </Link>
              : root.collapse &&
              <>
                <DropdownButton  className={`hover:bg-secondary/80 dark:hover:bg-dark-secondary 
            ${conditionCollapse(root.collapse) && ' bg-secondary dark:bg-dark-secondary'}`}
                  open={conditionCollapse(root.collapse)}
                  head={
                    <div className={`flex items-center gap-4  mb-2 py-3  ltr:pl-8  rtl:pr-8    ${conditionCollapse(root.collapse) && 'rtl:border-r-8 ltr:border-l-8 [border-color:rgba(var(--primary-color),1)] '} `}>
                      {root.icon && <div className="icon-container">
                        {root.icon}
                      </div>}
                      <span className={`text-base text-black dark:text-white`} >
                        {root.title}
                      </span>
                    </div>
                  }>
                  <ul className="p-4 bg-secondary dark:bg-dark-secondary ">
                    {root.list.map((li, index) => (
                      <li key={index}>
                        <Link href={{
                          pathname: li.href,
                          query: (router.query.account && type != "main") && { account: router.query.account },
                        }}>
                          <a className={`flex items-center gap-4 py-1 mb-1 md:mb-2 md:py-2 rtl:pr-2 ltr:md:pl-8 ltr:pl-2 rtl:md:pr-8  hover:bg-[#635e5e18] 
                      ${router.pathname === li.href ? 'rtl:border-r-8 ltr:border-l-8 [border-color:rgba(var(--primary-color),1)] [color:rgb(var(--primary-color))] ' : "text-black dark:text-white"
                            }`}>
                            {li.icon}
                            <span className={`text-base`}>
                              {li.title}
                            </span>
                          </a>
                        </Link>
                      </li>
                    ))}
                  </ul>
                </DropdownButton>
              </>

            }
          </li>
        ))}
      </ul>
      :
      <SelectPicker
        data={!props.collapse ? roots : changeCollapsRoots(roots)}
        cleanable={false}
        searchable={false}
        labelKey="title"
        renderMenuItem={(label, item) => {
          return (
            <div className="flex items-center gap-2 p-1">
              {props.collapse ?
                item.icon
                :
                <div className="icon-container">{item.icon}</div>
              }
              <div>{label}</div>
            </div>
          );
        }}
        renderValue={(value, item) => {
          return (
            <div className="flex items-center gap-2 p-1 sm:p-2">
              {item &&
                props.collapse ?
                item.icon
                :
                <div className="icon-container">{item && item.icon}</div>
              }
              <div>{item &&  item.title}</div>
            </div>
          );
        }}
        groupBy={props.collapse && "group"}
        valueKey="href"
        onChange={changeRoute}
        className="my-4 mx-4 w-[calc(100%_-_2rem)]"
        defaultValue={router.pathname}
      />
  )
}
