import React, { useState, useEffect, useContext, useRef, useCallback } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Notification, Translate, Sun1, Moon, Profile, HambergerMenu, Add, Warning2 } from 'iconsax-react'
import Link from "next/link"
import { Loader } from 'rsuite';
import { IconSaxName, SearchSite, DarkThemeButton, ChangeLangButton, NoData, Error, BackOverlay ,Loading} from "@/ui"
import toast from "react-hot-toast";
import profile from "public/images/placeholder/profile.png"
import Pusher from 'pusher-js';
import { format, differenceInSeconds } from 'date-fns'
import { generateLink, convertTime } from "libs/funcs"

export default function NotificationItems({ allNotificationCollect, errorAllNotification ,loading,activity}) {
  const { t, lang } = useTranslation("common")
    return (
        <>
            {loading ? <Loading  />
                : errorAllNotification ? <Error message={""} />
                    : allNotificationCollect && allNotificationCollect.length === 0 ? <NoData text={activity ? t("there_are_no_activities_yet") : t("there_are_no_notifications_yet")} />
                        : allNotificationCollect && allNotificationCollect.map((notification, index) => (
                            <li key={index}>
                                <Link href={generateLink(notification)}>
                                    <a className={`flex items-start gap-2 py-2 px-4 mb-1 hover:bg-secondary dark:hover:bg-dark-secondary `}>
                                        <div className="mb-2 icon-container">
                                            <IconSaxName name={notification.notification_image} size="18" />
                                        </div>
                                        {/* {index + 1} */}
                                        <div>
                                            <h3 className="mb-0 text-sm leading-none">{notification.notification_body}</h3>
                                            {/* <bdi className="text-xs text-gray-400">{convertTime(Math.abs(+differenceInSeconds(new Date(), new Date(notification.created_at))), format(new Date(notification.created_at), 'yyyy-MM-dd'))}</bdi> */}
                                            <bdi className="text-xs text-gray-400">{notification.created_at == "now" ? t("now") :notification.created_at }</bdi>
                                        </div>
                                    </a>
                                </Link>
                            </li>
                        ))}
        </>
    )
}
