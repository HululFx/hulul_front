import React from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Correct } from "public/svg"
import Image from "next/image"
import { Error, Loading } from "@/ui"
import { Navigation, Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import useWindowSize from "hooks/use-window";
export default function RealTypesAccounts({ data, error, accountType, setAccountType, change, setChange }) {
    const { t, lang } = useTranslation("dashboard")
    const size = useWindowSize();

    return (
        error ? <Error apiMessage={error} />
            : !data ? <div className="flex justify-center items-center min-h-[calc(100vh_-_20rem)]">< Loading /></div>:
                <div className="mb-8">
                    <Swiper
                        dir={lang === 'ar' ? "rtl" : "ltr"}
                        // loop="true"
                        slidesPerView={size.width > process.env.lg ? 3  :size.width > process.env.md ?2: 1}
                        spaceBetween={30}
                        navigation={true}
                        modules={[Navigation, Pagination]}
                        className="mySwiper real-types-accounts-slider px-4 lg:px-0"
                        pagination={size.width > process.env.md ? false : {
                            dynamicBullets: "true",
                        }}

                    >
                        {data.accountsTypes.length && data.accountsTypes.map((plan, index) => (
                            <SwiperSlide key={index} className="md:mb-0 mb-7">
                                <div key={index} className="px-8 py-6 my-4 bg-secondary dark:bg-dark-secondary  rounded-xl">
                                    <div className="">
                                        <div className="flex items-center justify-center w-12 h-12 p-1 rounded-full [background:rgba(var(--primary-color),1)] aspect-square absolute">
                                            <Image alt={plan.account_name} src={`${process.env.hostImage}/${plan.image}`} width="30" height="30" />
                                        </div>
                                        <h3 className="mt-1 lg:text-3xl text-lg  text-black dark:text-white capitalize text-center">{plan.account_name}<br /><span className="block text-xs text-center text-gray-400 ">{t("starting_from")}</span></h3>
                                    </div>
                                    <div className="mb-10 text-center">
                                        <bdi><span className="lg:text-3xl text-lg  font-black text-black dark:text-white">{plan.pips}</span>&nbsp;pips</bdi><br />
                                        <span className="text-gray-500">{plan.commission ? t("there_is_a_commission") : t("there_is_no_commission")}</span>
                                    </div>
                                    <ul className="mb-8 rtl:mr-4 ltr:ml-4 text-gray-500 ">
                                        <li className="relative flex justify-between pr-2 mb-4 before:w-3 before:h-3 before:[background:rgba(var(--primary-color),1)] before:absolute rtl:before:-right-4 ltr:before:-left-3 before:top-1 before:rounded-full">
                                            <span>{t("the_lowest_deposit_amount")}</span>
                                            <span className="font-bold text-black dark:text-white">{plan.min_deposit}</span>
                                        </li>
                                        <li className="relative flex justify-between pr-2 mb-4 before:w-3 before:h-3 before:[background:rgba(var(--primary-color),1)] before:absolute rtl:before:-right-4 ltr:before:-left-3 before:top-1 before:rounded-full">
                                            <span>EA</span>
                                            <span className="font-bold text-black dark:text-white">{plan.EA === "yes" ? t("yes") : t("no")}</span>
                                        </li>
                                        <li className="relative flex justify-between pr-2 mb-4 before:w-3 before:h-3 before:[background:rgba(var(--primary-color),1)] before:absolute rtl:before:-right-4 ltr:before:-left-3 before:top-1 before:rounded-full">
                                            <span>{t("less_trading_volume")}</span>
                                            <span className="font-bold text-black dark:text-white">{plan.Min_trading_volume}</span>
                                        </li>
                                        <li className="relative flex justify-between pr-2 mb-4 before:w-3 before:h-3 before:[background:rgba(var(--primary-color),1)] before:absolute rtl:before:-right-4 ltr:before:-left-3 before:top-1 before:rounded-full">
                                            <span>{t("islamic_account")}</span>
                                            <span className="font-bold text-black dark:text-white">{plan.Islamic_account === "yes" ? t("yes") : t("no")}</span>
                                        </li>

                                    </ul>
                                    <div className={`relative`}>
                                        <input name="account_type" type="radio" value={+plan.type_id} className="absolute top-0 right-0 w-full h-full opacity-0 peer" onChange={(event) => { setAccountType(event.target.value), setChange(change + 1) }} checked={+accountType === +plan.type_id} />
                                        <Correct size="15" className="absolute top-1/2 right-1/2 peer-checked:block hidden transform translate-x-1/2 -translate-y-1/2" />
                                        <div className={`bg-secondary dark:bg-dark-secondary  py-3  rounded-xl flex items-center justify-center  border peer-checked:border-2 peer-checked:[border-color:rgba(var(--primary-color),1)] [color:rgb(var(--primary-color))] peer-checked:[background:rgba(var(--primary-color),1)]  [border-color:rgba(var(--primary-color),1)]  `}>
                                            {t("choose_the_account")}
                                        </div>
                                    </div >
                                </div>

                            </SwiperSlide>
                        ))}
                    </Swiper>
                </div>
    )
}

