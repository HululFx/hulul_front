import Link from 'next/link'
import { Loader } from 'rsuite';

export default function ButtonTheme({ color, as, outline, href, size, type, block, disabled, loading, fontSize, ...props }) {
    const classes = `cursor-pointer button  text-center rounded-lg ${fontSize === "md" ? "text-xl" : "text-base"} text-white leading-none ${outline ? `out-primary ${color === "primary" && 'md:hover:[background:rgba(var(--primary-color),1)]'} md:hover:text-white` : `${color === "primary" && 'md:hover:[background:rgba(var(--primary-color),0.6)]'}  transition`} ${props.className}  ${block && 'w-full block'} ${size === "xs" ? "py-4 md:px-6 px-4" : "md:py-6 py-4 md:px-6 px-4"} ${disabled ? "bg-[#959595] pointer-events-none" : `${color === "primary" ? "[background:rgba(var(--primary-color),1)]" : color} md:hover:text-white `}`
    //color : primary ,
    //as    : Link|| button(submit)
    //outline: to change style to outline
    // href : if as== link we need to href
    //size : sm md lg
    //type : type button
    //block : add full width and block (w-full block)
    //loading : loading when send api
    return (
        as == "link" ?
            <Link href={href} >
                <a className={classes} >
                    {props.children}
                </a>
            </Link>
            : as == "span" ?
                <span className={`${classes} ${loading ? "pointer-events-none" : ""}`} type={type} onClick={props.onClick}>
                    {loading ? <Loader /> : props.children}
                </span>
                :
                <button className={`${classes} ${loading ? "pointer-events-none" : ""}`} type={type} onClick={props.onClick}>
                    {loading ? <Loader /> : props.children}
                </button>
                

    )
}


