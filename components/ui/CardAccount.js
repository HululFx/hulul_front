import React, { useState } from 'react'
import { Menu, Pin } from "public/svg"
import { MoneyRecive, Add, ArrowSwapVertical, Trash, Setting4, Refresh2, MoneySend, Lock1, Warning2 } from 'iconsax-react';
import useTranslation from 'next-translate/useTranslation'
import Link from "next/link"
import { Loader } from 'rsuite';
import { ButtonTheme, BackOverlay } from "@/ui"
import { WarningModal, DepositModal } from "@/modals";
import { convertAccountToFixed, deleteDemoAccount } from "apiHandle"
import useWindowSize from "hooks/use-window";
export default function CardAccount({ type, data, handelFixed, handleDeleteDone }) {
    const size = useWindowSize();
    const { balance, currency, color, leverage, login, freeMargin, equity, id, fixed, lastActivities, Margin } = data
    const { t, lang } = useTranslation("dashboard")
    const [deleteAccount, setDeleteAccount] = useState(false)
    const [openRealDepositDraw, setOpenRealDepositDraw] = useState(false)
    const [typeMoveMoney, setTypeMoveMoney] = useState("")
    const [realDepositId, setRealDepositId] = useState(null)
    const [openMenu, setOpenMenu] = useState(false)
    const [loadingDeleteButton, setLoadingDeleteButton] = useState(false)
    const [loadingFixedButton, setLoadingFixedButton] = useState(false)
    const [openCardBody, setOpenCardBody] = useState(false)
    const handleFix = () => {
        setLoadingFixedButton(true);
        convertAccountToFixed({
            id: id,
            success: () => { setLoadingFixedButton(false); setOpenMenu(false); handelFixed(); },
            error: () => {setLoadingFixedButton(false);setOpenMenu(false);}
        })


    }
    const handleDelete = () => {
        setLoadingDeleteButton(true);
        deleteDemoAccount({
            login: login,
            success: (response) => { setLoadingDeleteButton(false); setOpenMenu(false); setDeleteAccount(false);handleDeleteDone();  },
            error: (err) => {  setLoadingDeleteButton(false); setOpenMenu(false); setDeleteAccount(false); }
        })
    }
    return (
        <>
            <div className={`col-span1 relative ${(Margin < 0) && '[filter:drop-shadow(0px_0px_5px_rgba(255,0,0,0.6))]'}`} >
                {/* //!change 1200 to capital */}
                {fixed === "1" && <span className="absolute top-0 flex items-center justify-center w-8 h-8 text-black transform translate-x-1/2 bg-white rounded-bl-lg rounded-br-lg right-1/2 z-1"><Pin fill="inherit" /></span>}
                <div className="bg-secondary dark:bg-dark-secondary rounded-xl" >
                    <div className={`  py-5 px-6 rounded-xl text-white relative bg-color`} style={{ "--color": color }} onClick={() => setOpenCardBody(!openCardBody)}>
                        <h5>{t("balance")}</h5>
                        <span className="block mb-1 text-4xl ">${balance}</span>
                        <span className="block mb-10 capitalize tracking-widest3 text-[#bfbfbf]">{type === "real" ? data.account_type_name : 'STANDARD'}</span>
                        <button className="absolute flex p-1 bg-white rounded-lg top-4 rtl:left-4 ltr:right-4" onClick={() => setOpenMenu(true)}>
                            <Menu />
                        </button>
                        {openMenu && <div className="absolute p-3 text-black bg-white rounded-lg z-3 dark:text-white dark:bg-dark-white top-4 rtl:left-4 ltr:right-4">
                            <button onClick={() => setOpenMenu(false)} className="absolute top-2 rtl:left-2 ltr:right-2">
                                <Add className=" flex text-[#E30000] transform rotate-45 bg-white dark:bg-dark-white  rounded-xl" size="25 " />
                            </button>
                            {type === "demo" ? <Link href={`/dashboard/${type}/${id}/deposit?account=${id}`}>
                                <a className="flex items-center gap-2 px-3 py-2 mb-1 cursor-pointer hover:[background:rgba(var(--primary-color),0.1)]"><MoneyRecive className="w-5 text-black dark:text-white" size="25" />{t("deposit")}</a>
                            </Link>
                                :
                                <button onClick={() => { setOpenRealDepositDraw(true); setRealDepositId(id); setTypeMoveMoney("deposit") }} className="w-full flex items-center gap-2 px-3 py-2 mb-1 cursor-pointer hover:[background:rgba(var(--primary-color),0.1)]"><MoneyRecive className="w-5 text-black dark:text-white" size="25" />{t("deposit")}</button>
                            }
                            {type === "real" &&
                                <button onClick={() => { setOpenRealDepositDraw(true); setRealDepositId(id); setTypeMoveMoney("draw") }} className="w-full flex items-center gap-2 px-3 py-2 mb-1 cursor-pointer hover:[background:rgba(var(--primary-color),0.1)]"><MoneySend className="w-5 text-black dark:text-white" size="25" />{t("draw")}</button>
                            }
                            <Link href={`/dashboard/record/closed-deals?login=${login}`}>
                                <a className="flex items-center gap-2 px-3 py-2 mb-1 cursor-pointer hover:[background:rgba(var(--primary-color),0.1)]"> <Refresh2 className="w-5 text-black dark:text-white" size="25" />{t("record")}</a>
                            </Link>
                            <Link href={`/dashboard/${type}/${id}/account-information?account=${id}`}>
                                <a className="flex items-center gap-2 px-3 py-2 mb-1 cursor-pointer hover:[background:rgba(var(--primary-color),0.1)]"><Setting4 className="w-5 text-black dark:text-white" size="25" />{type === "real" ? t("account_modification") : t("account_modification")}</a>
                            </Link>
                            {/* {type === "real" && <li className="flex items-center gap-2 px-3 py-2 mb-1 cursor-pointer hover:[background:rgba(var(--primary-color),0.1)]"> <Lock1 className="w-5 text-black dark:text-white" size="25" />{t("change_password")}</li>} */}
                            <button disabled={loadingFixedButton} className="flex items-center w-full gap-2 px-3 py-2 mb-1 cursor-pointer hover:[background:rgba(var(--primary-color),0.1)]" onClick={handleFix}>{loadingFixedButton ? <Loader /> : <Pin className="w-5 text-black dark:text-white" fill="inherit" />}{fixed === "1" ? t("cancel_account_installation") : t("account_installation")}</button>
                            {type != "real" && <button onClick={() => setDeleteAccount(true)} disabled={loadingDeleteButton} className="flex items-center w-full gap-2 px-3 py-2 mb-1 cursor-pointer hover:bg-red-600 hover:text-white"><Trash className="w-5 text-inherit" size="25" />{t("delete_account")}</button>}
                        </div>}
                        <ul className="flex justify-between">
                            <li>
                                <div className="flex items-center gap-2">
                                    <span className="p-1 rounded-lg bg-white/30 dark:bg-dark-white/10">
                                        <ArrowSwapVertical size="20" />
                                    </span>
                                    <span>
                                        <h6 className="text-sm leading-none text-[#bfbfbf]">MT5</h6>
                                        <span className="text-base">{currency}</span>
                                    </span>
                                </div>
                            </li>
                            <li>
                                <h6 className="text-sm leading-none text-[#bfbfbf]">{t("leverage")}</h6>
                                <bdi className="text-base">1:{leverage}</bdi>
                            </li>
                            <li>
                                <h6 className="text-sm leading-none text-[#bfbfbf]">{t("account_number")}</h6>
                                <span className="text-base tracking-widest2">{login}</span>
                            </li>
                        </ul>
                    </div>
                    {(openCardBody || size.width > process.env.lg) && <div className="p-4 border-gray-300">
                        <ul className="flex justify-between py-4 border-b border-gray-300">
                            <li>
                                <h6 className="text-sm mb-1 text-[#bfbfbf]">{t("current_balance")}</h6>
                                <bdi className="text-sm font-bold text-black dark:text-white">{equity}$</bdi>
                            </li>
                            <li>
                                <h6 className="text-sm mb-1 text-[#bfbfbf]">{t("the_margin_reserved")}</h6>
                                <bdi className="text-sm font-bold text-black dark:text-white">{Margin}$</bdi>
                            </li>
                            <li>
                                <h6 className="text-sm mb-1 text-[#bfbfbf]">{t("available_margin")}</h6>
                                <span className="text-sm font-bold text-black dark:text-white">{freeMargin}$</span>
                            </li>
                        </ul>
                        <div className="py-4">
                            {lastActivities.length ? <h6 className="mb-2 text-xs">{t("the_last_account_movements")}</h6> :<br/>}
                            <ul className="2xl:h-40 xl:h-[11rem] h-40 overflow-y-auto">
                                {lastActivities.length ? lastActivities.map((activity, index) => (
                                    <li key={index} className="relative mb-4 after:rounded-xl after:[background:rgba(var(--primary-color),1)] after:absolute after:w-1 after:h-7 after:top-1 rtl:after:right-0 ltr:after:left-0 rtl:pr-2 ltr:pl-2 ">
                                        <p className="text-base font-bold leading-none text-black dark:text-white">{activity.activity}</p>
                                        <small>{activity.created_at}</small>
                                    </li>
                                ))
                                    :
                                    <li className="flex flex-col justify-center items-center gap-4 h-full">
                                        <Warning2 className="w-12 h-12"/>
                                        <p className="text-base font-bold leading-none text-black dark:text-white">{t("there_are_no_movements_yet")}</p>
                                    </li>
                                }
                            </ul>
                        </div>
                    </div>}
                </div>
            </div>
            <WarningModal open={deleteAccount} onClose={() => setDeleteAccount(false)} message={
                <>
                    <p className="mb-4 text-xl font-bold text-black dark:text-white">{t("do_you_want_to_delete_the_account")}</p>
                    <div className="flex justify-between gap-4 p-1 my-8">
                        <ButtonTheme loading={loadingDeleteButton} color="primary" onClick={handleDelete} className="w-1/2 px-4 py-2">{t("yes_delete_now")}</ButtonTheme>
                        <ButtonTheme color="primary" outline onClick={() => setDeleteAccount(false)} className="w-1/2 px-4 py-2">{t("no_cancel_the_deletion")}</ButtonTheme>
                    </div>
                </>
            } />
            <DepositModal open={openRealDepositDraw} type={typeMoveMoney} onClose={() => setOpenRealDepositDraw(false)} id={realDepositId} />

            {openMenu && <BackOverlay onClick={() => setOpenMenu(false)} />}
        </>
    )
}

