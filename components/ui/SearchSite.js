import React , {useState} from 'react'
import { SearchNormal1} from 'iconsax-react'
import useTranslation from 'next-translate/useTranslation'
export default function SearchSite({className}) {
    const { t, lang } = useTranslation("common")
  return (
    <div className={`flex-grow ${className}`}>
    <form>
      <div className="flex gap-2">
        <button type="submit">
          <SearchNormal1 className="text-gray-400" />
        </button>
        <input placeholder={t('i_am_looking_for_something')} type="search" className="w-full bg-transparent" />
      </div>
    </form>
  </div>
  )
}
