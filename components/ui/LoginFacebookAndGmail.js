import React, { useState, useEffect } from 'react'
import { loginGoogle1, loginGoogle2, loginFacebook1, loginFacebook2 } from "apiHandle"
import { Facebook, Google } from 'iconsax-react';
import useTranslation from 'next-translate/useTranslation'
import { useRouter } from 'next/router'
import {loginUser ,successLogin} from "apiHandle"

export default function LoginFacebookAndGmail() {
    const router = useRouter()
    const { t, lang } = useTranslation("auth")
    const [googleLoginUrl, setGoogleLoginUrl] = useState("")
    const [facebookLoginUrl, setFacebookLoginUrl] = useState("")
  
    useEffect(() => {
        // for google
        loginGoogle1({
            success: (response) => {
                setGoogleLoginUrl(response.data.url);
            },
            error: (err) => {
            }
        })
        // for facebook

        loginFacebook1({
            success: (response) => {
                setFacebookLoginUrl(response.data.url);
            },
            error: (err) => {
            }
        })

        // login google after redirect
        if (router.query.authuser) {
            loginGoogle2({
                params: `${router.asPath.slice(17)}`,
                success: (response) => {
                    
                    successLogin(response)
                },
                error: (err) => {
                    
                }
            })
        } else if (router.query.code) {
            loginFacebook2({
                params: `${router.asPath.slice(17)}`,
                success: (response) => {
                    
                    successLogin(response)
                },
                error: (err) => {
                    
                }
            })
        }
        // login facebook after redirect
    }, [router]
    )
    return (
        <div className="flex md:justify-between justify-center md:gap-4 gap-8 my-2 md:mb-4 mb-8 mt:mt-10">
            <a href={facebookLoginUrl} className={` ${!facebookLoginUrl && "pointer-events-none	"} flex items-center md:w-1/2 w-max gap-2  text-blue-400 border rounded md:rounded-lg p-4`}><Facebook size="20" className="text-blue-400" /><span className="hidden md:block">{t('register_via_facebook')}</span></a>
            <a href={googleLoginUrl} className={` ${!googleLoginUrl && "pointer-events-none	"} flex items-center md:w-1/2 w-max gap-2  text-black border rounded md:rounded-lg p-4`}><Google size="20" className="text-black" /><span className="hidden md:block">{t('register_via_google')}</span></a>
        </div>
    )
}
