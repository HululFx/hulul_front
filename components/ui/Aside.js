import React, { useState, useEffect } from 'react'
import { Logo, SmallAside ,SearchSite , DarkThemeButton ,ChangeLangButton,BackOverlay} from "@/ui"
import useTranslation from 'next-translate/useTranslation'
import { useRouter } from 'next/router'
import { LogoutModel } from "@/modals"
import { LogoutCurve, Add } from 'iconsax-react'
import useAuth from 'libs/useAuth'

export default function Aside({ roots, openAside, closeAsideHandler }) {
  const { logout } = useAuth({ middleware: 'auth' })
  const router = useRouter()
  const { t, lang } = useTranslation("aside")
  const [openLogout, setOpenLogout] = useState(false)
  const [loadingLogout, setLoadingLogout] = useState(false)
  const handleLogout = () => {
    setLoadingLogout(true);
    logout({
      success: () => { setOpenLogout(false); setLoadingLogout(false); },
      error: () => { setLoadingLogout(false); setOpenLogout(false) }
    })
  }
  useEffect(()=>{
    closeAsideHandler()
  },[router])
  // {
  //   title: t('sign_out'), icon: <LogoutCurve size="18" className="[color:rgb(var(--primary-color))] " />, click: true
  // },
  return (
    <>
      <div className={`bg-white dark:bg-dark-white xl:w-[288px]  w-[384px] z-10 max-w-full ${openAside ? "block" : "hidden"}  xl:block grid-area-home-aside fixed h-full overflow-auto `}>
        <div className=" items-center justify-center p-4 mb-0 xl:flex hidden">
          <Logo big />
        </div>
        <button className="xl:hidden icon-container  flex justify-center items-start p-0-important relative w-10 h-10 my-6 rtl:mr-auto ltr:mr-auto rtl:ml-4 ltr:ml-4" onClick={() => closeAsideHandler()}>
          <Add size="40" className="transform rotate-45 text-danger text-2xl" />
        </button>
        <div className="p-4 m-4 bg-secondary dark:bg-dark-secondary rounded-xl xl:hidden">
        <SearchSite className="xl:hidden"/>
        </div>
        <SmallAside steps={3} type="main" roots={roots} />
        <br />
        <button onClick={() => setOpenLogout(true)} className={`w-full flex items-center gap-4  mb-2 py-4  ltr:pl-8  rtl:pr-8  hover:bg-secondary/80 dark:hover:bg-dark-secondary `}>
          <div className="icon-container">
            <LogoutCurve size="18" className="[color:rgb(var(--primary-color))] " />
          </div>
          <span className="text-base text-black dark:text-white" >
            {t('sign_out')}
          </span>
        </button>
      <ul className="flex items-start gap-4 p-6">
      <ChangeLangButton className="lg:hidden "/>
      <DarkThemeButton className="lg:hidden "/>
      
      </ul>
      </div >

      <LogoutModel open={openLogout} onClose={() => setOpenLogout(false)} handleLogout={handleLogout} loading={loadingLogout} />
      {openAside && <BackOverlay  onClick={()=>closeAsideHandler()}/>}

    </>

  )
}
