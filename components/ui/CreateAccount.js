import React from 'react'
import { DashedBorder } from "public/svg"
import {  Add } from 'iconsax-react';
import {useRouter} from 'next/router'

export default  function CreateAccount({text , href , type ,allTrue, handleOpenModal}){
  
  const router = useRouter();
  const handleChangeRoute=()=>{
    if(type === "demo" || allTrue){
      router.push(href) 
    }else{
      handleOpenModal()
    }
   
  }
    return (
      <div className="relative flex items-center justify-center mb-14">
      <DashedBorder width="760" height="157"/>
      <button onClick={handleChangeRoute} >
        <a className="relative flex md:flex-col gap-2 lg:gap-0 items-center w-full lg:p-4 p-6  lg:text-xl text-sm rounded-xl [color:rgb(var(--primary-color))] ">
          <Add
            className="[color:rgb(var(--primary-color))]w-7 h-7 md:h-16 md:w-16"
          />
          <span>
            {text}
          </span>
        </a>
      </button>
    </div>
    )
  }