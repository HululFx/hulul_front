import React, { useState, useEffect, useContext, useRef, useCallback } from 'react'
import useTranslation from 'next-translate/useTranslation'
import { Notification, Translate, Sun1, Moon, Profile, HambergerMenu, Add, Warning2 } from 'iconsax-react'
import Image from "next/image"
import Link from "next/link"
import { Loader } from 'rsuite';
import usePersistLocaleCookie from "hooks/use-persist-locale-cookie"
import PermitionContext from "store/permition-context";
import useSWR, { useSWRConfig } from 'swr'
import { CheckUserData, getNotificationNumber, getAllNotification, getAllNotificationAsc, convertNotificationToRead } from "apiHandle"
import { IconSaxName, SearchSite, DarkThemeButton, ChangeLangButton, NoData, Error, BackOverlay, NotificationItems, ButtonTheme } from "@/ui"
import toast from "react-hot-toast";
import Pusher from 'pusher-js';
import { format, differenceInSeconds } from 'date-fns'
import { generateLink, convertTime } from "libs/funcs"
import { useRouter } from 'next/router'
export default function TopNav({ openAsideHandler }) {
  const { t, lang } = useTranslation("common")
    const router = useRouter()
  const ctx = useContext(PermitionContext);
  const [pageNotification, setPageNotification] = useState(1);
  const [sort, setSort] = useState('dec');
  const { data, error } = useSWR(CheckUserData())
  const { data: notificationNumber, error: errorNotificationNumber } = useSWR(getNotificationNumber())
  // const { data: allNotification, error: errorAllNotification } = useSWR(getAllNotification(pageNotification))
  const [allNotification, setAllNotification] = useState([]);
  const [errorAllNotification, setErrorAllNotification] = useState(false);
  const [loadingMore, setLoadingMore] = useState(false);
  const [loadingNotification, setLoadingNotification] = useState(false);
  const [openNotification, setOpenNotification] = useState(false);
  const [allNotificationCollect, setAllNotificationCollect] = useState([]);
  const [total, setTotal] = useState(0);
  const [lastNotificationNumber, setLastNotificationNumber] = useState(0);
  const [noMore, setNoMore] = useState(false);
  const [anewPusher, setAnewPusher] = useState({});
  const { mutate } = useSWRConfig();
  const notificationList = useRef(null);
  useEffect(() => {
    if (notificationNumber) {
      setLastNotificationNumber(notificationNumber.notifications_num)
    }
  }, [notificationNumber])
  const updateData = (sort, pageNotification, success, error) => {
    if (sort == "asc") {
      getAllNotificationAsc({
        page: pageNotification,
        success: (res) => { setLoadingNotification(false); success(res.data.data, res.data.total) },
        error: (err) => { setLoadingNotification(false); setErrorAllNotification(err); error() },
      })
    } else {
      getAllNotification({
        page: pageNotification,
        success: (res) => { setLoadingNotification(false); success(res.data.data, res.data.total) },
        error: (err) => { setLoadingNotification(false); setErrorAllNotification(err); error() },
      })
    }
  }
  // get first notification in start page , change sort 
  useEffect(() => {
    setLoadingNotification(true);
    updateData(sort, pageNotification, (data, total) => {
      setAllNotification(data);
      setAllNotificationCollect(data);
      setTotal(total)
    }, () => { })
  }, [setAllNotification, sort]);


  // save ctx values
  useEffect(() => {
    if (data) {
      
      // maybe change in use auth to avoid duplicate api
      ctx.changeUserInfo({
        "email": data.values[0].email,
        "phone": data.values[0].phone,
        "financialProfile": data.values[0].financialProfile,
        "identity_confirmation": data.values[0].identity_confirmation,
        "Address_confirmation": data.values[0].Address_confirmation,
        "all_basicInfo": data.values[0].all_basicInfo,
        "userImage": data.values[0].image,
        "name": data.user.name,
        "email_field": data.user.email,
        "phone_field": data.user.phone,
        "country": data.user.country,
      })
    }
  }, [data]);

  useEffect(() => {
    // get at more last 4 notification --done
    // always sort dec --done
    // if sort last keep dec and just push new --done
    // push last from pusher at more last 4 notification --done

    // first load with dec sort
    if (allNotification && sort == "dec" && Object.keys(anewPusher).length === 0) {
      ctx.changeLastActivities([...allNotification].slice(0, 4))
      // get new pusher 
    } else if (allNotification && sort == "dec" && Object.keys(anewPusher).length != 0) {
      ctx.changeLastActivities([anewPusher, ...allNotification].slice(0, 4))
      // setAnewPusher({})
    } else if (Object.keys(anewPusher).length != 0) {
      ctx.changeLastActivities([anewPusher, ...ctx.lastActiveties].slice(0, 4))
      // setAnewPusher({})
    }
    ctx.changeErrorlastActiveties(errorAllNotification)


  }, [allNotification, anewPusher]);

  useEffect(() => {
    // increase number
    if (Object.keys(anewPusher).length != 0) {
      setLastNotificationNumber(lastNotificationNumber + 1)
      if(sort == "dec"){
        setAllNotificationCollect([anewPusher, ...allNotificationCollect])
      }
    }

  }, [anewPusher])
  //for pusher 
  useEffect(() => {
    Pusher.logToConsole = true;

    const pusher = new Pusher('c568dc6eb5ee1b3a2ad9', {
      cluster: 'ap2',
      encrypted: true

    });

    const channel = pusher.subscribe(`notifications_${localStorage.userId}`);
    channel.bind('Notifications', function (data) {
      // alert("data")
      // allMessages.push(data);
      // setMessages(data);
      var newNotification = data;
      newNotification.created_at = "now";
      newNotification.body = Array.isArray(data.body) ? data.body[lang == "ar" ? 0 : 1][lang] : data.body  ;
      newNotification.notification_body = Array.isArray(data.body) ? data.body[lang == "ar" ? 0 : 1][lang] : data.body ;
      newNotification.notification_image = data.image;
      newNotification.is_read = '0';
      setAnewPusher(newNotification)

      toast.custom((t) => (

        <div
          className={`${t.visible ? 'animate-enter' : 'animate-leave'
            } max-w-md w-full bg-white shadow-lg rounded-lg pointer-events-auto flex ring-1 ring-black ring-opacity-5 overflow-hidden`}
        >
          <Link href={generateLink(newNotification)}>
            <div className="flex-1 w-0 p-4">
              <div className="flex items-start gap-4">
                <div className="flex-shrink-0 pt-0.5">
                  <IconSaxName name={newNotification.notification_image}  className="w-10 h-10 rounded-full" />
                </div>
                <div className="flex-1 ml-3">
                  <p className="text-sm font-medium text-gray-900">
                    {newNotification.body}
                  </p>
                </div>
              </div>
            </div>
          </Link>
          <div className="flex justify-start border-l border-gray-200">
            <button
              onClick={() => toast.dismiss(t.id)}
              className="flex items-center justify-center w-full p-4 text-4xl font-medium border border-transparent rounded-none rounded-r-lg text-danger h-max"
            >
              <Add size="40" className="transform rotate-45" />
            </button>
          </div>
        </div>
      ))
    });
  }, []);


  useEffect(() => {
    if (+total <= allNotificationCollect.length && !loadingMore) {
      setNoMore(true)
    } else {
      setNoMore(false)
    }
  }, [loadingMore, allNotificationCollect, total])
  useEffect(()=>{
    document.body.classList.remove("overflow-hidden")
    setOpenNotification(false)
  },[router])
  const HandleUpdate = () => {
    if (!loadingMore && +total > allNotificationCollect.length) {
      setLoadingMore(true)
      updateData(sort, pageNotification + 1, (data, total) => {
        setLoadingMore(false)
        setAllNotificationCollect([...allNotificationCollect, ...data]);
        
        
        
        setPageNotification(pageNotification + 1);
        setTotal(total)

      }, () => {
        setLoadingMore(false);
        toast.error("errToast:sorry_a_problem_occurred")
      })
    }
  }
  const handleOpenNotification = () => {
    document.body.classList.toggle("overflow-hidden")
    setOpenNotification(!openNotification);
    if(sort == "dec"){    
      convertNotificationToRead({
        success: (res) => {
          setLastNotificationNumber(0)
        },
        error: (err) => {
  
        },
      })
    }
  }
  return (
    <>
      <div className="flex-grow text-black bg-white dark:bg-dark-white dark:text-white grid-area-home-top">
        <div className="p-4 mx-auto container2">
          <div className="flex items-center justify-end xl:justify-between">
            <SearchSite className="hidden xl:block" />
            <ul className="flex items-start gap-4">
              <DarkThemeButton className="hidden xl:block " />
              <ChangeLangButton className="hidden xl:block " />
              <li className="relative ">
                <button className="relative flex items-start justify-center w-10 h-10 icon-container" onClick={handleOpenNotification}>
                  <Notification className="[color:rgb(var(--primary-color))] w-7 h-6" />
                  <span className={`${lastNotificationNumber === 0 && "hidden"} absolute flex items-center justify-center w-8 h-8 text-white transform rounded-full rtl:-right-3/4 ltr:-left-3/4 top-1/3 rtl:-translate-x-1/2 ltr:translate-x-1/2 bg-danger`}>
                    {!notificationNumber ? <Loader size="xs" />
                      : errorNotificationNumber ? "!"
                        : notificationNumber && +lastNotificationNumber > 99 ? '+99' : lastNotificationNumber}
                  </span>
                </button>
                <div className={`${!openNotification ?" max-h-0":" max-h-[calc(100vh_-_7rem)]"}  overflow-hidden transition-[max-height] ease-in-out delay-150 duration-100 md:absolute fixed md:top-12 top-16 ltr:right-0 rtl:left-0 z-10  rounded-xl  text-white  overflow-y-auto w-[25rem] bg-white dark:bg-dark-white`} ref={notificationList}>
                  <div className="flex items-center justify-between px-4 mb-6 text-black lg:px-6 dark:text-white">
                    <h4 className="text-2xl ">{t("notifications")}</h4>
                    <select className={`block w-[9.375rem]  px-4 py-4  rounded-md focus:outline-0 bg-secondary dark:bg-dark-secondary`} value={sort} onChange={(e) => { setSort(e.target.value); setPageNotification(1); setNoMore(false) }}>
                      <option value="dec">{t("the_most_recent")}</option>
                      <option value="asc">{t("the_oldest")}</option>
                    </select>
                  </div>
                  <ul className="p-0 m-0 text-black dark:text-white" >
                    <NotificationItems allNotificationCollect={allNotificationCollect} errorAllNotification={errorAllNotification} loading={loadingNotification} activity={false}/>
                    {(!loadingNotification && !errorAllNotification) &&
                      <>
                        {!noMore ?
                          <ButtonTheme onClick={HandleUpdate} color="primary" as="button" type="submit"  size="xs" className="mx-4 my-6 text-center xs:my-4 w-[calc(100%_-_2rem)]" loading={loadingMore}>
                            {t('loading_more')}
                          </ButtonTheme> :
                           allNotificationCollect.length > 10 && <span className="block text-center">{t("no_more_notifications")}</span>}
                      </>
                    }
                  </ul>
                </div>
              </li>
              <li className="hidden leading-none xl:block">
                {ctx.userInfo.userImage ?
                <div className="relative w-10 h-10">
                  <Image src={`${process.env.hostImage}/${ctx.userInfo.userImage}`} layout="fill" alt="profile" className="rounded-xl" />
                </div>
                  :
                  <div className="flex items-center justify-center w-10 h-10 [background:rgba(var(--primary-color),1)] rounded-xl" >
                    <Profile className="text-white " size="30" />
                  </div>
                }
              </li>
              <li className="leading-none xl:hidden">
                <button className="relative flex items-start justify-center w-10 h-10 icon-container" onClick={() => openAsideHandler()}>
                  <HambergerMenu className="[color:rgb(var(--primary-color))] " />
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    
      {openNotification && <BackOverlay onClick={() => {setOpenNotification(false);document.body.classList.remove("overflow-hidden")}} />}

    </>
  )
}
