import React, { useState } from 'react'
import setLanguage from 'next-translate/setLanguage'
import useTranslation from 'next-translate/useTranslation'
import {BackOverlay} from "@/ui"
import { useRouter } from 'next/router'
export default function ChangeLangButton({ className }) {
      const router = useRouter()
  const { t, lang } = useTranslation("common")
  const [shoWLangMenu, setShoWLangMenu] = useState(false);
  const [langSelect, setLangSelect] = useState(lang || "ar")
  const langs = [
    {
      "label": t('arabia'),
      "value": "ar",
    },
    {
      "label": t('english'),
      "value": "en",
    }
  ]
  const handleChangeLang = async (lang) => {
    setLangSelect(lang)
    if (lang === "ar") {
      await setLanguage('ar'); document.documentElement.dir = "rtl"
      router.reload()
    } else {
      await setLanguage('en'); document.documentElement.dir = "ltr"
      
      router.reload()
    }
  }


  return (
    <>
      <li className={className}>
        <div role="button" onClick={() => setShoWLangMenu(!shoWLangMenu)} className="icon-container  flex justify-center items-start  relative w-10 h-10">
          <div className="[color:rgb(var(--primary-color))] text-xl font-bold leading-3 select-none">
            ع
          </div>
            {shoWLangMenu && <BackOverlay  onClick={() => setShoWLangMenu(false)}/>}
          <div className={`absolute lg:top-full bottom-full rtl:right-0 ltr:left-0 h-max  text-black dark:text-white py-4 rounded-xl w-28 z-10-important bg-white dark:bg-dark-white ${!shoWLangMenu && "hidden"} `}>
            <div className={`${langSelect == "ar" && "hover:bg-secondary/80 dark:hover:bg-dark-secondary  rtl:border-r-4 ltr:border-l-4 [border-color:rgba(var(--primary-color),1)] bg-secondary dark:bg-dark-secondary"}`}>
              <button onClick={() => { handleChangeLang('ar'); setShoWLangMenu(false) }}><span className="p-2 px-4 block">{t('arabia')}</span></button>
            </div>
            <div className={`${langSelect == "en" && "hover:bg-secondary/80 dark:hover:bg-dark-secondary  rtl:border-r-4 ltr:border-l-4 [border-color:rgba(var(--primary-color),1)] bg-secondary dark:bg-dark-secondary"}`}>
              <button onClick={() => { handleChangeLang('en'); setShoWLangMenu(false) }}><span className="p-2 px-4 block">{t('english')}</span></button>
            </div>
          </div>
        </div>
      </li>
    </>
  )
}
