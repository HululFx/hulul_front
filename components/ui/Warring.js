import React, { useState, useEffect } from 'react'
import { Warning2, Add } from 'iconsax-react';
import useTranslation from 'next-translate/useTranslation'
import Link from "next/link"
import useNotCompleteInfo from "hooks/use-not-complete-info"
import { IconSaxName } from "@/ui"
import toast from "react-hot-toast";

export default function Warring() {
    const notComplete = useNotCompleteInfo()
    const [show, setShow] = useState(false)
    const [showNotification, setShowNotification] = useState(false)
    const { t, lang } = useTranslation("dashboard")

    useEffect(() => {
        setShow(notComplete)
        setShowNotification(notComplete)
    }, [notComplete])
    useEffect(() => {
        if (showNotification) {
        const interval = setInterval(() => {
                toast.custom((s) => (
                    <div
                        className={`${s.visible ? 'animate-enter' : 'animate-leave'
                            } max-w-md w-full bg-white shadow-lg rounded-lg pointer-events-auto flex ring-1 ring-black ring-opacity-5 overflow-hidden`}
                    >
                        <Link href="/dashboard/profile/personal/profile-personally">
                            <div className="flex-1 w-0 p-4">
                                <div className="flex items-start gap-4">
                                    <div className="flex-shrink-0 pt-0.5">
                                        <IconSaxName name={"profile-remove"} className="w-10 h-10 rounded-full" />
                                    </div>
                                    <div className="flex-1 ml-3">
                                        <p className="text-sm font-medium text-gray-900">
                                            {t("your_profile_is_not_complete_yet")}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </Link>
                        <div className="flex justify-start border-l border-gray-200">
                            <button
                                onClick={() => toast.dismiss('toastId')}
                                className="flex items-center justify-center w-full p-4 text-4xl font-medium border border-transparent rounded-none rounded-r-lg text-danger h-max"
                            >
                                <Add size="40" className="transform rotate-45" />
                            </button>
                        </div>
                    </div>
                ), {
                    id: "toastId"
                  })
            }, 1800000);
            return () => clearInterval(interval); 
        }
        
    }, [showNotification]);


    return (
        <>
            {show &&
                <div className="bg-white rounded-xl">
                    <div className="relative flex flex-col gap-2 p-3 mb-6 text-black [background:rgba(var(--primary-color),0.6)] lg:p-4 lg:flex-row rounded-xl lg:items-center ">
                        <div className="flex gap-2">
                            <Warning2 />
                            {t("your_profile_is_not_complete_yet")}
                        </div>
                        <Link href="/dashboard/profile/personal/profile-personally"><a className="block p-2 mx-2 bg-white rounded w-max">{t("go_to_the_profile")}</a></Link>
                        <button>
                            <Add className="transform rotate-45  absolute rtl:left-4 ltr:right-4 lg:top-[calc(50%-16px)] top-4 w-[1.875rem] h-[1.875rem]" onClick={() => setShow(false)} />
                        </button>
                    </div>
                </div>}

        </>
    )
}
