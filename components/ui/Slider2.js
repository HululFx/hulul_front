import React, { useState } from 'react'
import { ArrowLeft2, ArrowRight2 } from 'iconsax-react';
import { CardAccountTop } from "@/ui"
import useTranslation from 'next-translate/useTranslation'
import useWindowSize from "hooks/use-window";
import {  Navigation ,Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

export default function Slider2({ data, type, chooseSlide: slide, handleChooseIndexSlide }) {
    const size = useWindowSize();
  const { t, lang } = useTranslation("common")
  // const [chooseSlide, setChooseSlide] = useState(+slide)
  const handleCLickSlider = (i) => {
    // setChooseSlide(i)
    handleChooseIndexSlide(i)
  }

  return (
    <div className="mb-6 relative h-52">
      <div className=" h-full absolute left-0 right-0 w-full mx-auto t-0">
        <Swiper
          dir={lang === 'ar' ? "rtl" : "ltr"}
          // loop="true"
          slidesPerView={size.width > process.env.md  ? 2 : 1}
          spaceBetween={30}
          navigation={true}
          onSlideChange={(e) => {handleCLickSlider(e.snapIndex);}}
          modules={[ Navigation,Pagination]}
          className="mySwiper deposit-slider md:rtl:pr-6 md:ltr:pl-6 "
          pagination={size.width > process.env.md  ? false : {
            dynamicBullets: "true",
          }} 
         
        >
          {data.map((account, i) => (
            // <div className={` ${chooseSlide === i ? active : "opacity-70 transform scale-75  "} ${chooseSlide === i + 1 ? next : chooseSlide === i - 1 ? prev : "left-0 right-0 "} absolute 	 mx-auto h-full w-[20rem] transition duration-75	  `} onClick={() => handleCLickSlider(i)} key={i}>
            <SwiperSlide key={i} className="md:mb-0 mb-7">
              <CardAccountTop data={account} type={type} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  )
}