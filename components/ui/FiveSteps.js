import Link from "next/link"
import { Login } from 'iconsax-react';
import useWindowSize from "hooks/use-window";
import { Swiper, SwiperSlide } from "swiper/react";
import { EffectFade,Navigation } from "swiper";

import useTranslation from 'next-translate/useTranslation'
export default function FiveSteps() {
  const size = useWindowSize();

  const { t, lang } = useTranslation("dashboard")
  return (
    size.width > process.env.lg ?
      <Swiper
        dir={lang === 'ar' ? "rtl" : "ltr"}
        // loop="true"
        effect={"fade"}
        autoplay={{
          delay: 2500
        }}
        navigation={true}
        pagination={{
          clickable: true,
        }}
        modules={[EffectFade,Navigation]}
        className="mySwiper five-steps-slider"
      >
        {Array.from({ length: Number.parseInt(5) }, (item, index) => (
          <SwiperSlide key={index}> 
          <section className="p-8 mb-6 text-white rounded-lg bg-gradient-to-bl from-[rgba(var(--primary-color),1)] to-rose-500 md:rounded-xl hidden lg:block">
            <h2 className="mb-6 text-2xl font-bold">{t("with_5_simple_steps_start_trading")} {index + 1}</h2>
            <Link href="/"><a className="flex justify-end gap-2 text-white">{t("start_now")} <Login className="text-white transform rotate-180" size="20" /></a></Link>
          </section>
          </SwiperSlide>
        ))}
      </Swiper>

      : <></>
  )
}