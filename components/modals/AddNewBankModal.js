import React from 'react'
import { MoneyRecive, Wallet3, CardReceive, Clock, Add } from 'iconsax-react'
import useTranslation from 'next-translate/useTranslation'
import { Modal } from 'rsuite';
import ButtonTheme from '@/ui/ButtonTheme';
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { Input, InputIcon, CustumnCheckbox, SelectWIthHead, CustomnCheckColors, CustomnBalance,InputDate } from "@/form"

export default function AddNewBankModal({ open, onClose }) {
    const { t, lang } = useTranslation("depositAndDraw")
    const onSubmit = () => {

    }
    return (
        <Modal backdrop={true} keyboard={true} open={open} onClose={() => onClose()}>
            <Modal.Header>
                <Modal.Title>
                    <div className="flex items-center gap-2 mb-8 p-6">
                        <div className=" icon-container">
                            <Add size="35" className="[color:rgba(var(--primary-color),0.4)]" />
                        </div>
                        <h2 className="block lg:text-3xl text-lg font-bold text-black dark:text-white ">{t("add_a_new_card")}</h2>
                    </div>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body >
                <div className=" px-4">
                    <Formik
                        //   validationSchema={stepOneValidationSchema}
                        //   initialValues={props.data}
                        onSubmit={onSubmit}
                    >
                        {(props) => (
                            <Form>
                                <h2 className="text-xl mb-4 text-gray-600 font-bold ">{t("card_information")}</h2>
                                <Input name="name" type="text" placeholder={t('the_name_on_the_card')} />
                                <Input name="number" type="text" placeholder={t('card_number')} />
                                <div className="grid grid-cols-5 gap-8">
                                <Input name="cvv" type="text" placeholder="cvv / cvc" className="col-span-2" />
                                <InputDate name="date" type="text" placeholder="cvv / cvc" className="col-span-3" />
                                </div>
                                <ButtonTheme type="submit" color="primary" block size="xs" className="mt-10">{t("add_now")}</ButtonTheme>
                            </Form>
                        )}
                    </Formik>
                </div>
            </Modal.Body>
        </Modal>

    )
}
