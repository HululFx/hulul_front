import React from 'react'
import { Information } from 'iconsax-react'
import useTranslation from 'next-translate/useTranslation'
import { Modal } from 'rsuite';
import {ButtonTheme} from "@/ui"
export default function LogoutModel({ open, onClose, size ,handleLogout, loading }) {
const {t}= useTranslation("dashboard")
    return (
        <Modal backdrop={true} keyboard={true} open={open} onClose={() => onClose()} className={size == "sm" ? "less-size-modal" : "more-size-modal"}>
            <Modal.Header>
                <Modal.Title></Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="text-center px-4">
                <Information  className="mx-auto mb-6 text-orange-500 more-linear w-20 h-20 lg:w-36 lg:h-36"/>
                    <p className="mb-4 font-bold text-black dark:text-white lg:text-xl text-lg">{t("are_you_sure_to_log_out")}</p>
                    <div className="flex my-8 justify-between p-1 gap-4">
                        <ButtonTheme loading={loading} color="primary" onClick={handleLogout} className="w-1/2 px-4 py-2">{t("yes")}</ButtonTheme>
                        <ButtonTheme color="primary" outline onClick={() => onClose()} className="w-1/2 px-4 py-2">{t("cancel")}</ButtonTheme>
                    </div>
                </div>
            </Modal.Body>
            <Modal.Footer>

            </Modal.Footer>
        </Modal>

    )
}
