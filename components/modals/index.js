import WaitModal from "./WaitModal"
import WarningModal from "./WarningModal"
import DoneModal from "./DoneModal"
import DepositModal from "./DepositModal"
import LogoutModel from "./LogoutModel"
import AddNewBankModal from "./AddNewBankModal"
export {
    WaitModal ,WarningModal ,DoneModal,DepositModal,LogoutModel,AddNewBankModal
}