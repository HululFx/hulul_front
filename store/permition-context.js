import React, { useState } from "react";
export const PermitionContext = React.createContext({
  prop: "",
  userInfo:[],
  func: () => {},
  count: 0,
  changeUserInfo:()=>{},
  changeLastActivities:()=>{},
  IncCount: () => {},
  lastActiveties:[],
  errorlastActiveties:false,
  changeErrorlastActiveties:()=>{},
});
//you can dont write initial obj just for ide
export const PermitionContextProvider = (props) => {
  const [count, setCount] = useState(0);
  const [userInfo, setUserInfo] = useState({});
  const [lastActiveties, setLastActiveties] = useState([]);
  const [errorlastActiveties, setErrorlastActiveties] = useState([]);
  const changeUserInfo=(data)=>{
    setUserInfo(data)
  }
  const changeLastActivities=(data)=>{
    setLastActiveties(data)
  }
  const changeErrorlastActiveties=(data)=>{
    setErrorlastActiveties(data)
  }
  const IncCount = () => {
    setCount(count + 1);
  };
  const func = () => {
  };
  return (
    <PermitionContext.Provider
      value={{
        prop: "001",
        func: func,
        count: count,
        IncCount: IncCount,
        changeUserInfo,
        userInfo,
        lastActiveties,
        changeLastActivities,
        errorlastActiveties,
        changeErrorlastActiveties
      }}
    >
      {props.children}
    </PermitionContext.Provider>
  );
};
export default PermitionContext;
// note name of js       is cabab-case
//              function is UpperCase
