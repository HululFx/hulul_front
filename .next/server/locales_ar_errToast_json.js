"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
exports.id = "locales_ar_errToast_json";
exports.ids = ["locales_ar_errToast_json"];
exports.modules = {

/***/ "./locales/ar/errToast.json":
/*!**********************************!*\
  !*** ./locales/ar/errToast.json ***!
  \**********************************/
/***/ ((module) => {

module.exports = JSON.parse('{"sorry_a_problem_occurred":"عذرا حدثت مشكلة ما","sorry_email_previously_used":"عذرا البريد الإلكتروني تم أخذه سابقا","sorry_phone_previously_used":"عذرا  رقم الهاتف تم أخذه سابقا","sorry_the_email_is_not_true":"عذرا الايميل غير صحيح","sorry_the_wrong_password":"عذرا كلمة السر خاطئة","sorry_the_e_mail_is_not_used":"عذرا لا يمكن ايجاد الايميل المستخدم","sorry_the_phone_is_not_used":"عذرا لا يمكن ايجاد الهاتف المستخدم","we_have_emailed_your_password_reset_link":"لقد أرسلنا عبر البريد الإلكتروني رابط إعادة تعيين كلمة السر الخاصة بك","sorry_please_re_appoint_the_password_setd_set":"عذرا يرجى اعادة طلب تعيين كلمة السر","sorry_the_problem_of_what_will_be_re_request_will_be_re_appointed":"عذرا حدثت مشكلة ما سيتم اعادة طلب تعيين كلمة السر","the_word_has_been_successfully_changed":" تم تغيير كلمة بنجاح","please_wait_and_return_later":" يرجى الانتظار والمعاودة لاحقا","sorry_there_was_an_error_in_sending_the_code_please_return_later":" عذرا حدث خطأ في ارسال  الكود يرجى المعاودة لاحقا","sorry_the_code_is_wrong":" عذرا الرمز خطأ","sorry_the_account_was_confirmed_in_advance":" عذرا تم تأكيد الحساب مسبقا","please_enter_the_correct_phone_number":"يرجى ادخال رقم هاتف صحيح","please_write_the_code":"يرجى كتابة الكود","account_successfully_created":"تم إنشاء الحساب بنجاح","sorry_a_problem_has_occurred_in_downloading_the_image":"عذرا حدثت مشكلة في تحميل الصورة","the_data_has_been_successfully_saved":"تم حفظ البيانات بنجاح","new_password_cannot_be_same_as_your_current_password":"لا يمكن أن تكون كلمة المرور الجديدة هي نفسها كلمة مرورك الحالية","sorry_you_cant_fill_your_financial_profile_info_more_than_one_time":"عذرا لأنك غير قادر على ملء معلومات ملفك الشخصي المالي أكثر من مرة ","sorry_the_account_is_not_present":"عذرا الحساب غير موجود","sorry_there_is_a_problem_with_connecting_to_the_internet":"عذرا يوجد مشكلة بالاتصال بالانترنت","the_deposit_was_successful":"تم الايداع بنجاح","sorry_the_old_password_cannot_be_used":"عذرا لا يمكن استخدام كلمة المرور القديمة"}');

/***/ })

};
;