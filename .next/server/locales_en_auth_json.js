"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
exports.id = "locales_en_auth_json";
exports.ids = ["locales_en_auth_json"];
exports.modules = {

/***/ "./locales/en/auth.json":
/*!******************************!*\
  !*** ./locales/en/auth.json ***!
  \******************************/
/***/ ((module) => {

module.exports = JSON.parse('{"sliderText1":"lorem Ips ","welcome_with_us":"welcome with us","register_as_an_individual":"Register as an individual","register_as_an_company":"Register as a company","back_to_the_home_page":"Back to the home page","labor_agreement":"labor agreement","risk_warning":"Risk Warning","legal_documents":"legal documents","sign_in":"sign in","create_a_new_account":"Create a new account","welcom_to_us":"اهلا وسهلا بك معنا","register_company_title":"أنشئ حسابك خاص بشركات","create_account":"انشئ الحساب","i_have_an_account_log_in":"لدي حساب ,تسجيل الدخول"}');

/***/ })

};
;