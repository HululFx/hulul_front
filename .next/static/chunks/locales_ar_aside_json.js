"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(self["webpackChunk_N_E"] = self["webpackChunk_N_E"] || []).push([["locales_ar_aside_json"],{

/***/ "./locales/ar/aside.json":
/*!*******************************!*\
  !*** ./locales/ar/aside.json ***!
  \*******************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

module.exports = JSON.parse('{"control_board":"لوحة التحكم","the_accounts":"الحسابات","record":"السجل","closed_deals":"صفقات مغلقة","open_deals":"صفقات مفتوحة","deposit_and_clouds":"الأيداع والسحب","promotion":"الترويج","tools":"أدوات","money_management":"إدارة الأموال","profile_personly":"الملف الشخصي","sign_out":"تسجيل الخروج"}');

/***/ })

}]);