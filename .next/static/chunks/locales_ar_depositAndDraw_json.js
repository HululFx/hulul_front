"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(self["webpackChunk_N_E"] = self["webpackChunk_N_E"] || []).push([["locales_ar_depositAndDraw_json"],{

/***/ "./locales/ar/depositAndDraw.json":
/*!****************************************!*\
  !*** ./locales/ar/depositAndDraw.json ***!
  \****************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

module.exports = JSON.parse('{"deposit_amount":"أيداع مبلغ","draw_amount":"سحب مبلغ","deposit":"أيداع","draw":"سحب","what_place_do_you_want_to_deposit_the_amount":"الى اي مكان تريد ايداع المبلغ؟","what_place_do_you_want_to_draw_the_amount":"الى اي مكان تريد سحب المبلغ؟","choose_the_real_deposit_account":"أختر الحساب الحقيقي للأيداع","choose_the_real_draw_account":"أختر الحساب الحقيقي للسحب","for_my_wallet":"لمحفظتي","real_accounts":"الحسابات الحقيقية","complete_the_process":"اكمل العملية","choose_the_deposit_method":"أختر طريقة الأيداع","choose_the_draw_method":"أختر طريقة السحب","electronic_portfolio":"المحفظة الالكترونية","deposit_from_the_broker":"الايداع بواسطة وسيط معتمد","draw_from_the_broker":"السحب بواسطة وسيط معتمد","deposit_from_the_electronic_wallet_electronic":"ايداع من المحفظة الالكترونية {{electronic}}","draw_from_the_electronic_wallet_electronic":"سحب من المحفظة الالكترونية {{electronic}}","credit_card":"بطاقة ائتمان","cross_currencies":"عملات مشفرة","another_way":"طريقة أخرى","bank_transfer":"حوالة بنكية","deposit_to_bank_transfer":"ايداع من حوالة بنكية","draw_to_bank_transfer":"سحب من حوالة بنكية","deposit_from_payment_card":"ايداع من  بطاقة دفع","draw_from_payment_card":"سحب من  بطاقة دفع","certified_broker":"وسيط معتمد","payment_card":"بطاقة الدفع","enter_the_deposit_amount":"أدخل مبلغ الأيداع","enter_the_draw_amount":"أدخل مبلغ السحب","add_a_new_card":"أضف بطاقة جديدة","you_will_get":"ستحصل على","gift":"هدية","add_a_gift_coupon":"اضف كوبون هدية","pay_now":"الدفع الأن","send_order":"أرسل الطلب","card_information":"معلومات البطاقة","the_name_on_the_card":"الأسم على البطاقة","card_number":"رقم البطاقة","add_now":"أضف الأن","bank_name":"أسم البنك","the_recipients_name":"اسم المستلم","account_number":"رقم الحساب","conversion":"عملة التحويل","please_enter_the_bank_name":" يرجى اخال اسم البنك","please_enter_the_recipient_name":" يرجى ادخال اسم المستلم","please_enter_the_account_number":" يرجى ادخال رقم الحساب","please_enter_the_transfer_currency":"يرجى ادخال عملة التحويل  ","please_enter_the_amount_transferred":"يرجى ادخال المبلغ ","please_enter_the_Remittance_notices":"يرجى ادخال اشعار الحوالة ","raise_the_hawk_poems":"رفع أشعار الحوالة","enter_the_amount_converted":"أدخل المبلغ الذي تم تحويله","enter_the_amount_you_want_to_draw":"أدخل المبلغ الذي تريد سحبه","i_am_looking_for_an_intermediary":"أبحث عن وسيط","al_adman_will_be_reviewed_within_a_few_hours_if_the_operation_is_successful_the_amount_will_be_deposited_in_your_portfolio":"سيتم المراجعة من قبل الأدمن خلال ساعات قليلة في حال نجاح العملية سيتم ايداع المبلغ في محفظتك","al_adman_will_be_reviewed_within_a_few_hours_if_the_operation_is_successful_the_amount_will_be_drawed_in_your_portfolio":"سيتم المراجعة من قبل الأدمن خلال ساعات قليلة في حال نجاح العملية سيتم سحب المبلغ في محفظتك","and_if_there_is_a_problem_we_will_communicate_with_you":"وفي حال كان هناك مشكلة سنتواصل معك"}');

/***/ })

}]);