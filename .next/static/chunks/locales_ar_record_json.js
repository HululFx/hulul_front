"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(self["webpackChunk_N_E"] = self["webpackChunk_N_E"] || []).push([["locales_ar_record_json"],{

/***/ "./locales/ar/record.json":
/*!********************************!*\
  !*** ./locales/ar/record.json ***!
  \********************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

module.exports = JSON.parse('{"record":"السجل","trading":"حجم التداول","the_total_profit":"إجمال الربح","the_total_loss":"إجمال الخسارة","net_profit_and_loss":"صافي الربح والخسارة","search":"البحث","today":"اليوم","the_account":"الحساب","operation_number":"رقم العملية","the_operation":"العملية","currency_type":"نوع العملة","the_opening_price":"سعر الأفتتاح","the_closure_price":"سعر الأغلاق","profit":"الربح","sale":"بيع","buy":"شراء","the_total_profit_and_loss":"إجمال الربح والخسارة","total_deposit":"أجمالي الأيداع","the_total_clouds":"إجمال السحب","withdrawal":"سحب","your_bank_account":"حسابك البنكي","my_wallet":"محفظتي","all":"الكل","from":"من","to_me":"الى","the_value_of_the_amount":"قيمة المبلغ","operation_time":"وقت العملية","please_enter_the_search":"يرجى ادخال البحث","the_today":"اليوم","the_week":"الأسبوع","the_month":"الشهر","the_year":"السنة","yesterday":"الأمس","last7Days":"اخر سبعة أيام","ok":"موافق","loading":"جاري التحميل","there_are_no_data":"لا يوجد بيانات","opening_time":"وقت الافتتاح","price":"السعر","print":"طباعة","excel":"اكسل","there_are_no_registered_data_yet":"لاتوجد بيانات مسجلة بعد","you_can_search_for_the_account_the_operation_number_or_the_type_of_currency":"يمكنك البحث عن الحساب او رقم العملية او نوع العملة","ascending_order":"ترتيب تصاعدي","descending_order":"ترتيب تنازلي","closed_deals":"صفقات مغلقة","open_deals":"صفقات مفتوحة","deposit_and_clouds":"الأيداع والسحب","filter_according_to":"فلترة حسب","All":"الكل"}');

/***/ })

}]);