import useSWR from 'swr'
import axios from "libs/axios";
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import Trans from 'next-translate/Trans'
import {sendRequest} from "apiHandle"
import toast from "react-hot-toast";

export default function useAuth({ middleware } = {}) {
    const router = useRouter()
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        if (user || error) {
            setIsLoading(false);
        }

        if (middleware == 'guest' && user) router.push('/dashboard')
        if (middleware == 'auth' && !user && error) router.push('/auth/register-all')
    })

    const { data: user, error, mutate } = useSWR('/user',
        () => axios.get('/Check-User-Data').then(response => response.data.user)
        // () => axios.get('/Check-User-Data').then(response => response.data.user).catch(err=>!err.response.status === 401 )
    )
    // const user="d";
    // const error =false
    // const csrf = () => axios.get('/sanctum/csrf-cookie')

   
  

    async function logout ({success,error}) {
        //! remove it
        localStorage.setItem("userType", "");
            localStorage.setItem("token", "");
            router.push('/auth/register-all');
        //! remove it

        await axios.post('/logout')
        .then(res=>{
            mutate(null)
            localStorage.setItem("userType", "");
            localStorage.setItem("token", "");
            router.push('/auth/register-all');
            success()
        })
        .catch(err=>{
            error()
        })

    }

    return {
        user,
        logout,
        isLoading
    }
}


// axios.get('/Check-User-Data')
// .then(response => response.data.user)
// .catch(function (err) {
//     if (err.response) {
//         if(err.message === "Network Error"){
//             return false
//             // error network
//             // toast.error(<Trans i18nKey="errToast:sorry_there_is_a_problem_with_connecting_to_the_internet"/>)
//         }else{
//             return err
//             // ! for test
//             toast.error(err.message);
//             toast.error(
//                 <Trans i18nKey="errToast:sorry_a_problem_occurred" />
//             )
//         }
//     } else {
//         toast.error(
//             <Trans i18nKey="errToast:sorry_a_problem_occurred" />
//         )
//     }
// })