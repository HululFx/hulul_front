// import axios from 'axios';

// export default axios.create({
//     baseURL: process.env.host,
//     headers: {
//         'X-Requested-With': 'XMLHttpRequest',
//     },
//     // withCredentials: true,
// });

import Axios from 'axios'

const TOKEN =
    typeof window !== 'undefined' ? localStorage.getItem('token') : null
const axios = Axios.create({
    baseURL: process.env.host,
    // timeout :5000,//5s
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded',
        "Authorization": `Bearer ${TOKEN}`,
        'X-localization':  typeof window !== 'undefined' ? document.documentElement.lang : 'en',
    },
    // withCredentials: true,
})

export default axios
