import Trans from 'next-translate/Trans'

export function generateLink(data) {
    let endUrl;
    switch (data.title) {
        case "Deposit-accept":
            endUrl = "#"
            break;
        case "Withdraw-accept":
            endUrl = "#"
            break;
        case "Deposit-Reject":
            endUrl = "#"
            break;
        case "Withdraw-Reject":
            endUrl = "#"
            break;
        case "deposited":
            endUrl = "#"
            break;
        case "withdrawn":
            endUrl = "#"
            break;
        case "accept-realAccount":
            endUrl = `/dashboard/real/${data.info.account_id}/account-information?account=${data.info.account_id}`
            break;
        case "reject-realAccount":
            endUrl = "/dashboard/real/create-account"
            break;
        case "accept-leverage-change":
            endUrl = `/dashboard/real/${data.info.account_id}/account-information?account=${data.info.account_id}`
            break;
        case "reject-leverage-change":
            endUrl = `/dashboard/real/${data.info.account_id}/account-information?account=${data.info.account_id}`
            break;
        case "accept-balance-change":
            endUrl = `/dashboard/real/${data.info.account_id}/account-information?account=${data.info.account_id}`
            break;
        case "reject-balance-change":
            endUrl = `/dashboard/real/${data.info.account_id}/account-information?account=${data.info.account_id}`
            break;
        case "accept-Address-confirmation-document":
            endUrl = `/dashboard/profile/upload-documents/confirm-the-address`
            
            break;
        case "accept-identity-confirmation-document":
            endUrl = "/dashboard/profile/upload-documents/identification-confirmation"
            break;
        case "reject-Address-confirmation-document":
            endUrl = "/dashboard/profile/upload-documents/confirm-the-address"
            break;
        case "reject-identity-confirmation-document":
            endUrl = "/dashboard/profile/upload-documents/identification-confirmation"
            break;
        case "create-demo-account":
            endUrl = `/dashboard/demo/${data.info.account_id}/account-information?account=${data.info.account_id}`
            break;
        case "change-demo-account-color":
            endUrl = `/dashboard/demo/${data.info.account_id}/account-information?account=${data.info.account_id}`
            break;
        case "change-real-account-color":
            endUrl = `/dashboard/real/${data.info.account_id}/account-information?account=${data.info.account_id}`
            break;
        case "delete-demo-account":
            endUrl = `/dashboard?page=1&tab=1`
            break;
        case "change-demo-password":
            endUrl = "#"
            break;
        case "change-real-password":
            endUrl = "#"
            break;
        case "change-password":
            endUrl = "#"
            break;
        case "change-demo-account-leverage":
            endUrl = `/dashboard/demo/${data.info.account_id}/account-information?account=${data.info.account_id}`
            break;
        default:
            endUrl = "#"
    }
    return endUrl;
}
export function convertTime(s, date) {

    if (s == 0 || s < 60) {
        return <Trans i18nKey="common:now" />;
    } else if (s < 3600) {
        return <Trans i18nKey="common:since_minute_count" values={{ count: Math.floor(s / 60) }} />;
    } else if (s < 68400) {
        return <Trans i18nKey="common:since_hour_count" values={{ count: Math.floor(s / 3600) }} />
    } else if (s < 1036800) {
        return <Trans i18nKey="common:since_day_count" values={{ count: Math.floor(s / 68400) }} />
    } else if (s < 7257600) {
        return <Trans i18nKey="common:since_week_count" values={{ count: Math.floor(s / 1036800) }} />
    } else if (s < 217728000) {
        return <Trans i18nKey="common:since_month_count" values={{ count: Math.floor(s / 7257600) }} />
    } else {
        return <Trans i18nKey="common:since_date" values={{ count: date }} />
    }
}